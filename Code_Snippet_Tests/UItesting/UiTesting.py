from PyQt5 import QtCore, QtGui, QtWidgets
from UICode.testing import Ui_Testing
from UICode.options import Ui_Form
import sys


def main():
    app = QtWidgets.QApplication(sys.argv)

    dialog = QtWidgets.QMainWindow()
    dilog = QtWidgets.QMainWindow()
    # diprog = TestingProg(dialog,dilog)

    # dialog.show()
    diprog = UserOptions(dilog,"Joshua",dialog)
    diprog.saveItems.clicked.connect(diprog.savevariables)
    dilog.show()

    sys.exit(app.exec_())


class TestingProg(Ui_Testing):
    def __init__(self, dialog, dilog):
        Ui_Testing.__init__(self)
        self.setupUi(dialog)
        self.dialog = dialog
        self.dilog = dilog
        self.actionGubbins.triggered.connect(self.testingFunction)

    def testingFunction(self):
        text1 = self.testingOne
        self.testingTwo = text1
        diprog = UserOptions(self.dilog, " Joshua", self.dialog)
        self.dilog.show()


class UserOptions(Ui_Form):
    def __init__(self, useroptions, username, testingdialog):
        # Setup a basic state for the user.
        Ui_Form.__init__(self)
        self.setupUi(useroptions)
        # self.dialog = dialog
        self.username = username
        self.useroptions = useroptions
        self.testingDi = testingdialog
        # self.saveItems.clicked.connect(self.savevariables)

    def savevariables(self):
        print("Gubs")
        print("Gubs Nubs")
        print(self.username)
        diprog = TestingProg(self.testingDi, self.useroptions)
        diprog.actionGubbins.hovered.connect(diprog.testingFunction)
        self.testingDi.show()

if __name__ == '__main__':
    main()
