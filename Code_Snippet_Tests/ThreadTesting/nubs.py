from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class PunchingBag(QObject):
    ''' Represents a punching bag; when you punch it, it
        emits a signal that indicates that it was punched. '''
    punched = pyqtSignal()

    def __init__(self):
        # Initialize the PunchingBag as a QObject
        QObject.__init__(self)

    def punch(self):
        ''' Punch the bag '''
        self.punched.emit()


@pyqtSlot()
def say_punched():
    ''' Give evidence that a bag was punched. '''
    print('Bag was punched.')

bag = PunchingBag()
# Connect the bag's punched signal to the say_punched slot
bag.punched.connect(say_punched)
for i in range(10):
    bag.punch()


class A(QObject):
    def __init__(self):
        super(A, self).__init__()

    sig = pyqtSignal()

    @pyqtSlot()
    def slot(self):
        print("received")

a = A()
a.sig.connect(a.slot)

for i in range(10):
    a.sig.emit()
    a.sig.emit()
    a.sig.emit()
