'''from threading import Timer

class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer     = None
        self.interval   = interval
        self.function   = function
        self.args       = args
        self.kwargs     = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False
from time import sleep

def hello(name):
    print("Hello %s!" % name)

print("starting...")
rt = RepeatedTimer(1, hello, "World") # it auto-starts, no need of rt.start()
try:
    sleep(5) # your long-running job goes here...
finally:
    rt.stop() # better in a try/finally block to make sure the program ends!'''

from PyQt5 import QtCore, QtWidgets


def main():
    import sys
    # Defines the application as an app within the windows environment.
    app = QtWidgets.QApplication(sys.argv)
    # noinspection PyUnusedLocal
    # Start the Main controller to start the entire program.
    controller = MainController()
    # Sets the close behaviour of the program.
    sys.exit(app.exec_())


class MainController:
    def __init__(self):
        self.thread_instance = TimerThread(self)
        self.thread_instance.start()
        self.timers = []

    def thread_func(self):
        print("Thread works")
        timer = QtCore.QTimer()
        timer.timeout.connect(timer_func)
        timer.start(1000)
        print(timer.remainingTime())
        print(timer.isActive())
        self.timers.append(timer)


class TimerThread(QtCore.QThread):
    def __init__(self, oldgubs):
        QtCore.QThread.__init__(self)
        self.thread_func = oldgubs

    def run(self):
        self.thread_func.thread_func()
        self.exec_()


def timer_func():
    print("Timer works")

if __name__ == '__main__':
    main()
