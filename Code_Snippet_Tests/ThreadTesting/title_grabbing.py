import threading
import time
from urllib.request import *
from bs4 import BeautifulSoup

start = time.time()
urls = ["http://forums.relicnews.com/showthread.php?234824-HW-Legacies-Part-IV/page6",
       "http://www.fanfiction.net/s/6072033/18/Dragon_Age_The_Crown_of_Thorns",
       "https://www.fanfiction.net/s/2857962/1/Browncoat-Green-Eyes",
       "https://www.youtube.com/watch?v=GoVLhUxhdSw"]


def fetch_url(url):
    html = web_content_return(url)
    soup = BeautifulSoup(html, "html.parser")
    title = soup.title.text
    print("'%s\' fetched in %ss" % (title, (time.time() - start)))


def web_content_return(url):
    # Attempt to request the html data, pretend to be requesting such data as a firefox browser
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
    # Open the URL as as a separate variable, save the response data.
    html = urlopen(req)
    # Read the data as html data into a separate variable.
    web_content = html.read()
    # Return the web content to the function.
    return web_content

threads = [threading.Thread(target=fetch_url, args=(url,)) for url in urls]
for thread in threads:
    thread.start()
for thread in threads:
    thread.join()

print("Elapsed Time: %s" % (time.time() - start))


