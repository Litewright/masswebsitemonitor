import json


def main():

    username = input("Please Enter Username: - ")
    password = input("Please Enter Password: - ")
    passval = False

    with open('userdetail.json', 'r') as outfile:
        # Attempt to perform this action, return an error if it fucks up.
        try:
            # Load the json file as data.
            data = json.load(outfile)
            # Bind the username and password into json variables and put them
            # into one single json holder for comparison.
            usernamejson = {'username': username}
            passwordjson = {'password': password}
            testdata = {"loginattempt": [usernamejson, passwordjson]}

            # Setup a loop to go over every account inside the datastream from the file.
            for account in data:
                # Each data object is comprised of both a password and a username, while
                # active, loop over both objects to compare them.
                for x in range(0, 1):
                    # If the login attempt variable, the username, matches the one from the password,
                    # Check the passwords.
                    if data[account][x] == testdata["loginattempt"][x]:
                        # Check if the passwords match or not.
                        if data[account][x + 1] == testdata["loginattempt"][x + 1]:
                            print("Match Found")
                            passval = True
        # Print the error that occurred.
        except Exception as err:
            print(err)

    if passval is True:
        file = open('usersettings.json', 'r')
        usersettings = json.load(file)
        file_location = usersettings[username][0]
        print(file_location.get('file_save_location'))

if __name__ == '__main__':
    main()
