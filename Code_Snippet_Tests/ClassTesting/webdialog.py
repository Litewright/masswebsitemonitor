from PyQt5 import QtWidgets, QtCore, QtWebKitWidgets, QtGui


class WebDialog(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setWindowTitle("Differential Results Viewer")
        self.text_editor = QtWebKitWidgets.QWebView(self)

        printButton = QtGui.QPushButton('Print')
        printButton.clicked.connect(self.onPrint)
        printPreviewButton = QtGui.QPushButton('Print Preview')
        printPreviewButton.clicked.connect(self.onPrintPreview)

        btnLayout = QtGui.QHBoxLayout()
        mainLayout = QtGui.QVBoxLayout()

        btnLayout.addWidget(printButton)
        btnLayout.addWidget(printPreviewButton)
        mainLayout.addWidget(self.text_editor)
        mainLayout.addLayout(btnLayout)

        self.setLayout(mainLayout)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    form = WebDialog()
    form.show()
    app.exec_()
