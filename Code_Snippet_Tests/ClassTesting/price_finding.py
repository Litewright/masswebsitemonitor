from bs4 import BeautifulSoup
from urllib.request import *
import re

def web_content_return(url, var="Url"):
    if "Url" in var:
        # Attempt to request the html data, pretend to be requesting such data as a firefox browser
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
        # Open the URL as as a separate variable, save the response data.
        html = urlopen(req)
        # Read the data as html data into a separate variable.
        web_content = html.read()
        # Return the web content to the function.
        return web_content

    elif "Thread" in var:
        # Attempt to request the html data, pretend to be requesting such data as a firefox browser
        request = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
        # Open the URL as as a separate variable, save the response data.
        response = urlopen(request)
        # Read the data as html data into a separate variable.
        web_content = response.read()
        # soup = BeautifulSoup(web_content, "html.parser")
        # title = soup.title.text
        head = str(len(web_content))
        # Return the web content to the function.
        return url, head

    elif "Header" in var:
        # Attempt to request the html data, pretend to be requesting such data as a firefox browser
        request = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
        # Open the URL as as a separate variable, save the response data.
        response = urlopen(request)
        # Read the data as html data into a separate variable.
        web_content = response.read()
        head = str(len(web_content))
        # Return the web content to the function.
        return head


web_content = web_content_return(str("https://www.amazon.co.uk/gp/product/B00TE4XSMA/ref=s9_simh_gw_g147_i1_r?pf_rd_m=A"
                                     "3P5ROKL5A1OLE&pf_rd_s=desktop-1&pf_rd_r=GRHPPE10V12MPG11C8ZR&pf_rd_t=36701&pf_rd"
                                     "_p=867551787&pf_rd_i=desktop"))
soup = BeautifulSoup(web_content, "html.parser")

txt='<span id="priceblock_ourprice" class="a-size-medium a-color-price">£35.98</span>'

re1='(£)'	# Any Single Character 1
re2='([+-]?\\d*\\.\\d+)(?![-+0-9\\.])'	# Float 1

rg = re.compile(re1+re2, re.IGNORECASE|re.DOTALL)

m = rg.findall(soup.text)

for vals in m:
    print(vals)

from collections import Counter
word_counts = Counter(m)
top_three = word_counts.most_common(3)
print(top_three)

# Year Three Project/MainCode/UserFiles/userdetail.json
# temp = "D:\\Users\\Gmandam\\Dropbox\\Code\\Python\\Year Three Project\\MainCode\\UserFiles\\userdetail.json"
import sys
print(sys.path)
sys.path.append("D:\\Users\\Gmandam\\Dropbox\\Code\\Python\\Year Three Project\\MainCode\\UserFiles")

with open('userdetail.json', 'r') as outfile:
    print("gubs")
# if m:
#     c1 = m.group(1)
#     float1 = m.group(2)
#     print("("+c1+")"+"("+float1+")"+"\n")

# print(soup.find_all(text=rg))
# print(soup.body.find_all(text=re.compile('^Price$')))
