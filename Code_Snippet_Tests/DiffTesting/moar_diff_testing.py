from difflib import unified_diff
import re


def main():
    file_location_one = "webpageDownload.html"
    file_location_two = "webpageDownload2.html"

    compare_file_one = open(file_location_one, "r")
    compare_file_two = open(file_location_two, "r")

    file_container_one = list()
    file_container_two = list()

    for line in compare_file_one.readlines():
        if line.isspace():
            print("Whitespace Bro")
        else:
            file_container_one.append(striphtml(line))

    for line in compare_file_two.readlines():
        if line.isspace():
            print("Whitespace Bro")
        else:
            file_container_two.append(striphtml(line))

    compare_file_one.close()
    compare_file_two.close()

    diff_one = unified_diff(file_container_one, file_container_two, lineterm='')

    text = (''.join(list(diff_one)))
    print(text)


def striphtml(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

if __name__ == '__main__':
    main()
