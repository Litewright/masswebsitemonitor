from difflib import unified_diff
from urllib.request import urlopen, Request
import re


def main():
    url = 'http://gmandam.com/diff-test/'
    save_file_location = 'D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html'
    save_location_two = 'D:\\Users\\Gmandam\\Desktop\\webpageDownload.html'
    text_save_one = 'D:\\Users\\Gmandam\\Desktop\\webpageDownload2.txt'
    text_save_two = 'D:\\Users\\Gmandam\\Desktop\\webpageDownload.txt'

    req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
    html = urlopen(req)
    web_content = html.read()
    f = open(save_file_location, 'wb')
    f.write(web_content)
    f.close()

    compare_file = open(save_location_two, 'r')
    compare_file2 = open(save_file_location, 'r')

    text_file = open(text_save_two, 'w')
    text_file_2 = open(text_save_one, 'w')

    for line in compare_file.readlines():
        text_file.write(striphtml(line))

    for line in compare_file2.readlines():
        text_file_2.write(striphtml(line))

    text_file.close()
    text_file_2.close()

    text_file = open(text_save_two, 'r')
    text_file_2 = open(text_save_one, 'r')

    diff = unified_diff(text_file_2.readlines(), text_file.readlines(), lineterm='')

    text_file.close()
    text_file_2.close()

    text = ''.join(list(diff))
    print(text)
    count = 0
    skiplist = list()

    for line in text.splitlines():
        if line.startswith(('-', '+')) is True:
            for x in range(0, len(text.splitlines()) - 1):
                var1 = line.replace('-', '').replace('+', '').replace('@', '').replace('\t', '')
                var2 = text.splitlines()[x].replace('-', '').replace('+', '').replace('@', '').replace('\t', '')
                if x != count:
                    if x not in skiplist:
                        if var1 == var2:
                            print("Differences detected: -")
                            print(line)
                            print(text.splitlines()[x])
                            skiplist.append(count)

        count += 1
    count = 0
    stringtest = list()
    for line in text.splitlines():
        if count not in skiplist:
            stringtest.append(line + "\n")
        count += 1

    print(text)


def striphtml(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

if __name__ == '__main__':
    main()


