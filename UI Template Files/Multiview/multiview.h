#ifndef MULTIVIEW_H
#define MULTIVIEW_H

#include <QMainWindow>

namespace Ui {
class MultiView;
}

class MultiView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MultiView(QWidget *parent = 0);
    ~MultiView();

private:
    Ui::MultiView *ui;
};

#endif // MULTIVIEW_H
