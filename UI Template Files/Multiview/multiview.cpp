#include "multiview.h"
#include "ui_multiview.h"

MultiView::MultiView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MultiView)
{
    ui->setupUi(this);
}

MultiView::~MultiView()
{
    delete ui;
}
