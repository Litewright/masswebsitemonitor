#-------------------------------------------------
#
# Project created by QtCreator 2016-04-29T13:13:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Multiview
TEMPLATE = app


SOURCES += main.cpp\
        multiview.cpp

HEADERS  += multiview.h

FORMS    += multiview.ui
