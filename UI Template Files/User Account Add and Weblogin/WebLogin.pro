#-------------------------------------------------
#
# Project created by QtCreator 2015-10-31T01:20:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WebLogin
TEMPLATE = app


SOURCES += main.cpp\
        weblogin.cpp \
    jsoninput.cpp

HEADERS  += weblogin.h \
    jsoninput.h

FORMS    += weblogin.ui \
    jsoninput.ui
