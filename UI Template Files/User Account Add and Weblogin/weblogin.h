#ifndef WEBLOGIN_H
#define WEBLOGIN_H

#include <QDialog>

namespace Ui {
class WebLogin;
}

class WebLogin : public QDialog
{
    Q_OBJECT

public:
    explicit WebLogin(QWidget *parent = 0);
    ~WebLogin();

private:
    Ui::WebLogin *ui;
};

#endif // WEBLOGIN_H
