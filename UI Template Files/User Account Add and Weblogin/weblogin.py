# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'weblogin.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WebLogin(object):
    def setupUi(self, WebLogin):
        WebLogin.setObjectName("WebLogin")
        WebLogin.resize(708, 384)
        WebLogin.setAcceptDrops(True)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../MainCode/UICode/Coffee_Acrobat.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        WebLogin.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(WebLogin)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.loginButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.loginButton.setFont(font)
        self.loginButton.setObjectName("loginButton")
        self.gridLayout.addWidget(self.loginButton, 2, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setFrameShadow(QtWidgets.QFrame.Raised)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.loginPassword = QtWidgets.QLineEdit(self.centralwidget)
        self.loginPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.loginPassword.setObjectName("loginPassword")
        self.gridLayout.addWidget(self.loginPassword, 1, 1, 1, 1)
        self.loginUsername = QtWidgets.QLineEdit(self.centralwidget)
        self.loginUsername.setObjectName("loginUsername")
        self.gridLayout.addWidget(self.loginUsername, 0, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        WebLogin.setCentralWidget(self.centralwidget)

        self.retranslateUi(WebLogin)
        QtCore.QMetaObject.connectSlotsByName(WebLogin)

    def retranslateUi(self, WebLogin):
        _translate = QtCore.QCoreApplication.translate
        WebLogin.setWindowTitle(_translate("WebLogin", "Login"))
        self.loginButton.setText(_translate("WebLogin", "Login"))
        self.label_2.setText(_translate("WebLogin", "Login: -"))
        self.label.setText(_translate("WebLogin", "Password: -"))

