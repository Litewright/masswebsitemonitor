#ifndef JSONINPUT_H
#define JSONINPUT_H

#include <QMainWindow>

namespace Ui {
class JSONInput;
}

class JSONInput : public QMainWindow
{
    Q_OBJECT

public:
    explicit JSONInput(QWidget *parent = 0);
    ~JSONInput();

private:
    Ui::JSONInput *ui;
};

#endif // JSONINPUT_H
