import unittest
import json
import difflib
import MainProject.main_project_version_three
from PyQt5 import QtWidgets, QtCore, QtWebKitWidgets, QtGui, QtTest
from MainProject.main_project_version_three import Ui_MainWindow


class TestingSuite(unittest.TestCase):

    # General Class tests
    def test_username_accurate(self):
        with open('userdetail.json', 'r') as outfile:
            data = json.load(outfile)
            valid_usr_pas = ["Joshua", "Password"]
            self.assertTrue(MainProject.main_project_version_three.username_check(valid_usr_pas[0], data))

    def test_username_invalid(self):
        with open('userdetail.json', 'r') as outfile:
            data = json.load(outfile)
            invalid_up = ["NotValid", "NotPassword"]
            with self.assertRaises(KeyError):
                MainProject.main_project_version_three.username_check( invalid_up[0], data)

    def test_no_change_password(self):
        # If no Pass Change
        self.assertEqual(MainProject.main_project_version_three.result_yes(65536), False)

    def test_change_password(self):
        # If change Pass
        self.assertEqual(MainProject.main_project_version_three.result_yes(16384), True)

    def test_getresponsecode(self):
        self.assertEqual(MainProject.main_project_version_three.getresponsecode("http://forums.relicnews.com/"
                                                                                "showthread.php?234824-HW-Legacies-Part"
                                                                                "-IV/page6"), 200)

    def test_getnoresponse(self):
        self.assertEqual(MainProject.main_project_version_three.getresponsecode("http://www.bbc.com/897087565"), 404)

    def test_retirevesettings(self):
        self.assertTrue(MainProject.main_project_version_three.retrievesettings("Joshua"))

    def test_noretrievesettings(self):
        with self.assertRaises(KeyError):
            MainProject.main_project_version_three.retrievesettings("Hello")

    # UserLogin Tests
    def test_password_accurate(self):
        with open('userdetail.json', 'r') as outfile:
            data = json.load(outfile)
            valid_usr_pas = ["Joshua", "Password"]
            self.assertTrue(MainProject.main_project_version_three.LoginProg.password_check(valid_usr_pas[0],
                                                                                            valid_usr_pas[1], data))

    def test_password_invalid(self):
        with open('userdetail.json', 'r') as outfile:
            data = json.load(outfile)
            invalid_up = ["NotValid", "NotPassword"]
            with self.assertRaises(KeyError):
                MainProject.main_project_version_three.LoginProg.password_check(invalid_up[0], invalid_up[1],
                                                                                data)

    # MainProgramCheck
    def test_length_url(self):
        self.assertFalse(MainProject.main_project_version_three.GuiProgram.check_contains
                         ("http://forums.relicnews.com/showthread.php?234824-HW-Legacies-Part-IV/page6"))

    def test_nolength_url(self):
        self.assertTrue(MainProject.main_project_version_three.GuiProgram.check_contains(""))

    def test_url_request(self):
        try:
            webline = "http://forums.relicnews.com/showthread.php?234824-HW-Legacies-Part-IV/page6"
            web_content_return(webline)
        except Exception:
            self.fail()

    def test_url_request1(self):
        import urllib.error
        with self.assertRaises(urllib.error.HTTPError):
            webline = "http://www.bbc.com/897087565"
            web_content_return(webline)

    def test_url_request2(self):
        import urllib.error
        with self.assertRaises(Exception):
            webline = "192.168.02"
            web_content_return(webline)

    def test_no_url_request(self):
        with self.assertRaises(ValueError):
            webline = ""
            web_content_return(webline)

    def test_readfiles(self):
        try:
            save_location = "D:\\Users\\Gmandam\\Desktop\\webpageDownload.html"
            MainProject.main_project_version_three.GuiProgram.read_files(save_location)
        except IOError:
            self.fail()

    def test_noreadfiles(self):
        with self.assertRaises(IOError):
            MainProject.main_project_version_three.GuiProgram.read_files("")

    def test_save_to_file(self):
        try:
            save_location = "D:\\Users\\Gmandam\\Desktop\\test_save.txt"
            MainProject.main_project_version_three.GuiProgram.save_to_file(save_location,
                                                                           web_content_return
                                                                           ("http://forums.relicnews.com/showthread."
                                                                            "php?234824-HW-Legacies-Part-IV/page6"))
        except OSError:
            self.fail()

    def test_no_savetofile(self):
        with self.assertRaises(Exception):
            save_location = 'D:\\Users\\Gmandam\\Desktop\\gubs\\'
            MainProject.main_project_version_three.GuiProgram.save_to_file(save_location,
                                                                           web_content_return
                                                                           ("http://forums.relicnews.com/showthread."
                                                                            "php?234824-HW-Legacies-Part-IV/page6"))

    def test_liststring_return(self):
        alist = ["1"]
        self.assertIs(MainProject.main_project_version_three.GuiProgram.liststring(alist), "1")

    def test_listring_noreturn(self):
        with self.assertRaises(Exception):
            alist = [1]
            self.assertIsNot(MainProject.main_project_version_three.GuiProgram.liststring(alist), "1")

    def test_returnhtmllist(self):
        urltest = "http://forums.relicnews.com/showthread.php?234824-HW-Legacies-Part-IV/page6"
        url = web_content_return(urltest)
        try:
            MainProject.main_project_version_three.GuiProgram.return_html_list(self, url, 0)
        except Exception:
            self.fail()

    def test_returnnohtmllist(self):
        urltest = 1
        try:
            MainProject.main_project_version_three.GuiProgram.return_html_list(self, urltest, 1)
        except Exception:
            self.fail()

    def test_striphtml(self):
        try:
            MainProject.main_project_version_three.GuiProgram.striphtml("")
        except Exception:
            self.fail()

    def test_nostriphtml(self):
        with self.assertRaises(Exception):
            urltest = "http://forums.relicnews.com/showthread.php?234824-HW-Legacies-Part-IV/page6"
            url = web_content_return(urltest)
            MainProject.main_project_version_three.GuiProgram.striphtml(url)

    def test_diffconvert(self):
        text1 = "Hello"
        text2 = "Hello"
        diff = MainProject.main_project_version_three.GuiProgram.liststring(difflib.unified_diff(text1,
                                                                                                 text2, lineterm=''))
        self.assertEqual(MainProject.main_project_version_three.GuiProgram.diffconvert(diff),"")

    def test_nodiffconvert(self):
        text1 = [1, 2, 3]
        with self.assertRaises(Exception):
            MainProject.main_project_version_three.GuiProgram.diffconvert(text1)

    def test_fullrun(self):
        with self.assertRaises(SystemExit):
            MainProject.main_project_version_three.main()

if __name__ == '__main__':
    unittest.main()

