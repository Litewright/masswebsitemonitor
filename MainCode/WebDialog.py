from PyQt5 import QtWidgets, QtCore, QtWebKitWidgets, QtGui
'''
Produces a Dialog to allow the user to see the Differential Results.
Does so via QMainWindow.
Main Viewer is a WebBrowser.
'''


class WebDialog(QtWidgets.QMainWindow):
    def __init__(self):
        # Setup a basic state for the user
        QtWidgets.QMainWindow.__init__(self)
        # Set the title for the Dialog.
        self.setWindowTitle("Differential Results Viewer")
        # Set the main widget to be a frame, to allow for the automatic resizing of the whole dialog
        self.frame = QtWidgets.QFrame()
        # Set the frame as the Central Widget
        self.setCentralWidget(self.frame)
        # Produce a WebView to allow for HTML to be used when producing my Differential Results
        self.text_editor = QtWebKitWidgets.QWebView(self)
        # Set the size of the widget and the main window
        self.text_editor.resize(400, 300)
        self.resize(400, 300)
        # Put the layout as a HBoxLayout
        self.layout = QtWidgets.QHBoxLayout()
        # Set the layout as our main layout.
        self.frame.setLayout(self.layout)
        # Add our text_editor widget to the layout.
        self.layout.addWidget(self.text_editor)