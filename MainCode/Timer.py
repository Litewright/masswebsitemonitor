from PyQt5 import QtCore, QtWidgets


class TimerThread(QtCore.QThread):
    def __init__(self, reference):
        QtCore.QThread.__init__(self)
        self.thread_func = reference

    def run(self):
        self.thread_func.thread_func()
        self.exec_()
