# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'jsoninput.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_JSONInput(object):
    def setupUi(self, JSONInput):
        JSONInput.setObjectName("JSONInput")
        JSONInput.resize(614, 410)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../MainCode/UICode/Coffee_Acrobat.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        JSONInput.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(JSONInput)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setFrameShadow(QtWidgets.QFrame.Raised)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.inputPassword = QtWidgets.QLineEdit(self.centralwidget)
        self.inputPassword.setObjectName("inputPassword")
        self.gridLayout.addWidget(self.inputPassword, 2, 1, 1, 1)
        self.inputUsername = QtWidgets.QLineEdit(self.centralwidget)
        self.inputUsername.setObjectName("inputUsername")
        self.gridLayout.addWidget(self.inputUsername, 0, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.jsonInputButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.jsonInputButton.setFont(font)
        self.jsonInputButton.setObjectName("jsonInputButton")
        self.gridLayout_2.addWidget(self.jsonInputButton, 1, 0, 1, 1)
        JSONInput.setCentralWidget(self.centralwidget)

        self.retranslateUi(JSONInput)
        QtCore.QMetaObject.connectSlotsByName(JSONInput)

    def retranslateUi(self, JSONInput):
        _translate = QtCore.QCoreApplication.translate
        JSONInput.setWindowTitle(_translate("JSONInput", "MainWindow"))
        self.label.setText(_translate("JSONInput", "Password: -"))
        self.label_2.setText(_translate("JSONInput", "Login: -"))
        self.jsonInputButton.setText(_translate("JSONInput", "Input JSON Login Details"))

