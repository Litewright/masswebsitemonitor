import json
import urllib.error
from difflib import *
from PyQt5 import QtWidgets, QtCore, QtWebKitWidgets, QtGui
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from PyQt5.QtPrintSupport import QPrintDialog, QPrinter, QPrintPreviewDialog
from urllib.request import *
from WebDialog import WebDialog
from Timer import TimerThread
from MultiViewGui import ThreadGather
from UICode.pythongui import Ui_MainWindow
from UICode.jsoninput import Ui_JSONInput
from UICode.weblogin import Ui_WebLogin
from UICode.options import Ui_Form
from UICode.multiview import Ui_MultiView
from bs4 import BeautifulSoup
from os import path


def main():
    import sys
    # Defines the application as an app within the windows environment.
    app = QtWidgets.QApplication(sys.argv)
    # noinspection PyUnusedLocal
    # Start the Main controller to start the entire program.
    controller = MainController()
    # Sets the close behaviour of the program.
    sys.exit(app.exec_())


'''
Initalise the login screen.
Bind them to local variables to avoid garbage collection.
Show the login screen.
'''


class MainController:
    def __init__(self):
        # Create a QMainWindow to display the Login Window.
        self.logindialog = QtWidgets.QMainWindow()
        # Initialise the LoginDialog screen with this UI file and Python Class.
        self.loginprog = LoginProg(self.logindialog)
        # Show the login dialog screen.
        self.logindialog.show()


'''
Sets up the LoginScreen for the user to log into.
It checks the json file for matches in username, and then in the password field.
If successful, it'll pass the user onto the main screen.
In future, it'll send the user onto their account to perform diffs and measures
'''


class LoginProg(Ui_WebLogin):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, logindialog):
        try:
            # Setup a basic state for the user.
            Ui_WebLogin.__init__(self)
            # Setup the linked UI file with the current class.
            self.setupUi(logindialog)
            # Connect the action listener on the UI file (for the button) to that of a function.
            # The function being that of logincheck.
            # Set the password textfield to be a series of password characters over plaintext.
            self.loginPassword.setEchoMode(QtWidgets.QLineEdit.Password)
            # Hold a reference to the QMainWindow that displays the login dialog.
            self.logindialog = logindialog
            # Create a variable to hold a reference to a QMainWindow to hold the GUIProgram.
            self.dialog = QtWidgets.QMainWindow()
            # noinspection PyUnusedLocal
            # Link the login button to the method known as logincheck.
            obj = self.loginButton.clicked.connect(self.logincheck)
            # Preinitalise the variable to hold a reference to the class variable
            self.prog = Ui_MainWindow()
        except Exception as err:
            print(err)

    # Function compares the input from the username and password fields to the input from the account json file.
    def logincheck(self):
        # Bind the text from these fields into variables.
        # username = self.loginUsername.text()
        # password = self.loginPassword.text()
        username = "Joshua"
        password = "Password"

        # Open up the userdetail.json file for reading as an outfile.
        # Iterate over the file as needed.
        try:
            # Year Three Project/MainCode/UserFiles/
            # Year Three Project/MainCode/UserFiles/userdetail.json
            # D:\Users\Gmandam\Dropbox\Code\Python\Year Three Project\MainCode\UserFiles\userdetail.json
            file_path = path.relpath("MainCode//UserFiles//userdetail.json")
            with open(file_path, 'r') as outfile:
                # Attempt to perform this action, return an error if it fucks up.
                try:
                    # Load the json file as data.
                    data = json.load(outfile)
                    # Check if the Username is set as a key or not, all user names are set as keys in the program
                    # If the key matches the entered username, check the password.
                    if username_check(username, data):
                        # Check if the password matches the recorded password for the username, return with an error
                        # if it does not.
                        if self.password_check(username, password, data):
                            # Hide the Login Screen
                            self.logindialog.hide()
                            # Initalise the class variable prog with GuiProgram and a few variables.
                            self.prog = GuiProgram(self.dialog, username)
                            # Show the dialog program.
                            self.dialog.show()
                        else:
                            # Inform the User that their Username or Password was incorrect
                            # noinspection PyTypeChecker,PyArgumentList
                            QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match"
                                                              )
                    else:
                        # Inform the User that their Username or Password was incorrect
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match")
                # Print the error that occurred.
                except Exception as err:
                    errtest = err.args[0]
                    if errtest == username:
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match")
                    else:
                        print(err)
        except Exception as err:
            print(err)

    @staticmethod
    def password_check(username, password, jsoninput):
        if password in jsoninput[username][1].get('password'):
            return True

'''
Allows the user to add new users as required.
Inputs this into the jsonfile listed based off input into the text fields.
Only has one function.
'''


class UserProg(Ui_JSONInput):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, userdialog):
        # Setup a basic state for the user.
        Ui_JSONInput.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(userdialog)
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of jsoninput.
        # noinspection PyUnusedLocal
        self.obj = self.jsonInputButton.clicked.connect(self.jsoninput)

    def jsoninput(self):
        # Binds the textfield inputs into variables for processing.
        username = self.inputUsername.text()
        password = self.inputPassword.text()
        # Attempt to add new user to the userjson file.
        try:
            # Open the JSON file for input as dict
            newaccount = json.load(open('MainCode//UserFiles//userdetail.json'))

            # Check if the current username exists or not.
            # If it doesn't ignore the error, it's expected.
            # If it does, execute the code below.
            try:
                # Set username to equal this value, if the username already exists within the json list
                # noinspection PyUnusedLocal
                if username_check(username, newaccount):
                    # Ask the User if they'd like to change the current password for that present user.
                    msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Change Password",
                                                   "Do you wish to change the Password?",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                    # Result is equal to what button the user pressed.
                    result = msgbox.exec_()
                    # If the user responds with their desire to change the password, do the change method
                    if result_yes(result):
                        self.save_password(username, password, newaccount)
                    # Else, return a message to the user that Password was not changed.
                    else:
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to change this "
                                                                               "users password.")
            # Catch any errors relating to the fact that the file doesn't exist or if the username isn't present.
            # If the error is an unexpected error, catch it and print the information.
            except Exception as err:
                # If the user doesn't exist, assume that the user wishes to add a new user, and add it.
                errtest = err.args[0]
                if errtest == username:
                    self.save_password(username, password, newaccount)
                else:
                    print(err)
        except Exception as err:
            print(err)

    # Save the Password and Username to the Json file containing usernames and passwords.
    @staticmethod
    def save_password(username, password, newaccount):

        old_websitewatch = json.load(open('MainCode//UserFiles//websitewatch.json', 'r'))
        old_options = json.load(open('MainCode//UserFiles//websitewatch.json', 'r'))

        # Bind the password and username into the correct format for usage.
        data = {username: [{'username': username}, {'password': password}]}
        websitewatch_info = {username: []}
        options_info = {username: {[]}}
        # Append the new data onto the old file data.
        newaccount.update(data)
        old_websitewatch.update(websitewatch_info)
        old_options.update(options_info)
        # Save the old data + the new data to the json file.
        json.dump(newaccount, open('MainCode//UserFiles//userdetail.json', 'w'))
        json.dump(old_websitewatch, open('MainCode//UserFiles//websitewatch.json', 'w'))
        json.dump(old_options, open('MainCode//UserFiles//websitewatch.json', 'w'))
'''
Main computing class as of present.
Handles performing basic diffs on urls and HTML web scraped files.
Allows web pages to be saved to a fixed location.
Allows the current user to move to other screens upon button click
'''


class GuiProgram(Ui_MainWindow):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, dialog, username):

        # Setup a basic state for the user.
        Ui_MainWindow.__init__(self)

        # Setup the linked UI file with the current class.
        self.setupUi(dialog)

        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of save.
        # noinspection PyUnusedLocal
        obj = self.urlButton.clicked.connect(self.save)

        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of compare.
        # noinspection PyUnusedLocal
        obj1 = self.compareButton.clicked.connect(self.compare)
        # Create some windows to allow users to view other forms when specific buttons are pressed.
        self.userdialog = QtWidgets.QMainWindow()
        self.userprog = Ui_JSONInput
        self.optionsdialog = QtWidgets.QMainWindow()
        self.optionsprog = Ui_Form
        self.diff_display = WebDialog()
        self.multiview = QtWidgets.QMainWindow()
        self.multiprog = Ui_MultiView
        # Set the class username variable to equal the username given to the class by the Login Screen
        self.username = username
        self.returnlist = list()
        # Set the save file location to the associated user location from the user options.
        self.file_save_location = retrievesettings(self.username)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of changeoptions.
        obj2 = self.actionEditOptions.triggered.connect(self.changeoptions)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of newusercreation.
        obj3 = self.actionAdd_New_User.triggered.connect(self.newusercreation)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of multiwebsiteview.
        obj4 = self.actionView_Multiwebsites.triggered.connect(self.multiwebsiteshow)

        obj5 = self.actionSave_Website_as_PDF.triggered.connect(self.save_pdf)

        obj6 = self.actionSend_Email_Alert.triggered.connect(self.send_email)

    def check_price(self):
        text, ok = QtWidgets.QInputDialog.getText(None, 'Input Dialog',
                                                 'Enter URL Address')

        if ok:
            web_content = web_content_return(str(text))
            soup = BeautifulSoup(web_content, "html.parser")
            soup.find_all('price')

    def send_email(self):
        self.compare()
        msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Send Info Email",
                                       "Do you wish to send an Email with the Differential Results?",
                                       QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        result = msgbox.exec_()
        if result == QtWidgets.QMessageBox.Yes:
            text, ok = QtWidgets.QInputDialog.getText(None, 'Input Dialog',
                                                     'Enter Email Address')

            if ok:
                import smtplib
                from email.mime.text import MIMEText
                sender = "PythonDiffer@swansea.com"
                reciever = [str(text)]
                message = """
                To :- User,
                Subject: - Differential Results
                    There are the following updates to the web differential"""
                for line in self.returnlist:
                    message += line

                try:
                    server = smtplib.SMTP("smtp.gmail.com:587")
                    server.starttls()
                    server.login("pythonicrobot@gmail.com", "nubsgubs29!")
                    # smtpObj = smtplib.SMTP('smtp.gmail.com')
                    server.sendmail(sender, receivers, message)
                    print("Successfully sent email")
                except smtplib.SMTPException:
                    print("ERROR")
            else:
                QtWidgets.QMessageBox.response(None, "You have chosen not to send an email.")
        else:
            QtWidgets.QMessageBox.response(None, "You have chosen not to send an email.")

    def save_pdf(self):
        text, ok = QtWidgets.QInputDialog.getText(None, 'Input Dialog',
                                                  'Enter Website:')
        if ok:
            try:
                url = str(text)
                web = QtWebKitWidgets.QWebView()
                web.load(QtCore.QUrl(url))
                printer = QPrinter()
                printer.setPageSize(QPrinter.A4)
                printer.setOutputFormat(QPrinter.PdfFormat)
                fname = QtWidgets.QFileDialog.getSaveFileName(None, 'save file')
                savename = fname[0] + ".pdf"
                printer.setOutputFileName(savename)
                web.print_(printer)
            except Exception as err:
                print(err)

    # Save the URL put inside lineEdit_2 to a file for further future manipulation.
    def save(self):
        # Set the url as the text contained within the lineEdit.
        url = self.selectUrl.text()
        # Check if there is any input at all, if not, throw up a warning.
        if self.check_contains(url):
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            try:
                web_content = web_content_return(url)
                # Save the web content as plaintext decoded into the utf-8 standard.
                plaintext = web_content.decode("utf-8")
                # Set the htmlViewer to display the text as plaintext
                # If you don't it'll attempt to display it as an actual webpage.
                self.htmlViewer.setPlainText(plaintext)
                msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Save Webpage",
                                               "Do you wish to save the Webpage?",
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                result = msgbox.exec_()
                if result_yes(result):
                    self.save_to_file("MainCode//WebsiteFiles//newsaveplace.html", web_content)
                else:
                    # noinspection PyTypeChecker,PyArgumentList
                    QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to save the webpage.")
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Respond with a message if the file location is invalid or you do not have user rights to save the file.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "File name or location invalid. Check current settings.")
            # Debug Code
            except Exception as err:
                print(err)

    # Scrape the web content from the given URL and save it.
    # Pass it along to another function to perform the actual differential.
    def compare(self):
        # Set the url as the text contained within the lineEdit.
        url = self.selectUrl.text()
        url = "http://gmandam.com/diff-test/"
        self.selectUrl.setText(url)
        # Set the file area for the already saved file to the input of lineEdit as a string.
        # file_area = "MainCode//WebsiteFiles//webpageDownload.html"
        file_area = QtWidgets.QFileDialog.getOpenFileName(None, "Open File")[0]
        self.fileSelect.setText(file_area)
        # Set the save location for the HTML code scraped to a fixed location
        save_file_location = 'MainCode//WebsiteFiles//webpageDownload2.html'
        # Check if there is any input at all, if not, throw up a warning.
        if self.check_contains(url):
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            try:
                web_content = web_content_return(url)
                self.save_to_file(save_file_location, web_content)
                # Perform another function to do the actual comparisons.
                self.comparefile(file_area, save_file_location)
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError as err:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
                print(err)
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Respond with an error if the file location is invalid or not.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Incorrect File Name Entered.")
            # Debug Code
            except Exception as err:
                print(err)

    # Performs the actual differential upon the webpage and file.
    # Report on the differences, or the lack of differences.
    def comparefile(self, file_area, save_file_location):
        import time
        start = time.clock()
        print("Start of Time Measure")
        # Check if there is any input at all, if not, throw up a warning.
        if self.check_contains(file_area):
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a File Location.")
        else:
            try:
                # Attempt to open the files for read only access in a binary format.
                compare_file = self.read_files(file_area)
                compare_file2 = self.read_files(save_file_location)
                # Setup some lists to hold the html pages as lists.
                file_container_one = self.return_html_list(compare_file, 0)
                file_container_two = self.return_html_list(compare_file2, 0)
                # Return file read cursor to the top of the file.
                compare_file2.seek(0)
                compare_file.seek(0)
                default_page = self.return_html_list(compare_file2, 1)
                original_page = self.return_html_list(compare_file, 1)
                # Close both open files.
                compare_file.close()
                compare_file2.close()

                # Perform a unix style diff on the files that are open.
                diff = unified_diff(file_container_one, file_container_two, lineterm="")
                # print()
                # If the second file is closed, delete it from the system.
                # Warning, sometimes there is a permissions error here in some IDEs.
                # if compare_file2.closed is True:
                #    import os
                #    os.remove('D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html')

                # If the Diff is empty, respond saying that there were no changes. Else respond that there
                # were changes.
                # text = self.diffconvert(self.liststring(list(diff)))
                text = ''.join(diff)
                # Ready a new list to host the diff output with our changes.
                # Side note, we need to do this so that we can break the diff output down line by line
                # and then edit it inline
                print(text)
                li = ''
                highlight_list = list()
                line_list = list()
                # Count the number of additions and removals from the source file to the current webpage.
                poscount = 0
                negcount = 0

                # This iterates over every line from the diff output allowing us to determine what exactly what
                # differences exist between the current webpage and the saved copy.
                for index, line in enumerate(text.splitlines()):

                    # If the line starts with a - or +, then perform these actions.
                    # This is important, all changes in the work will be highlighted with either a - or +.
                    # The first line, the one that showcases where the changes take place and the number of changes is
                    # marked with a @.
                    if line.strip().startswith(('-', '+')) is True:

                        # Determine if this is one of the lines that indicates where exactly the changes took place
                        # between the old and new files.
                        # TODO:- Make sure we calcualte the number of @ in this line to determine if we should care
                        # TODO:- about it or not.
                        if '@@' in line:

                            # Cleanup the line in order to filter out the unimportant data such as whitespace and the
                            # -,+ and @ characters.
                            cleanline = line.replace('-', '').replace('+', '').replace('@', '').strip().split(' ')

                            # Based off the cleanline list, rip out the values for the line locations that differ
                            # between the files. IMPORTANT NOTE: - The location should in theory always be the same due
                            # to the way the unified diff command works.
                            linestart = str(int(cleanline[0].split(',')[0]) + 1)
                            lineend = str(int(cleanline[1].split(',')[0]) + 1)

                            # Assigns the second half of the initial unified diff output into variables to calculate
                            # the actual end for where the line is.
                            worddiffa = int(cleanline[0].split(',')[1])
                            worddiffb = int(cleanline[1].split(',')[1])

                            # Calculate the actual line end, it's a calculation by removing the larger number from the
                            # smaller number. The logic below determines the larger number in order for the calculation
                            # to make sense.
                            answer = 0
                            if worddiffa - worddiffb < 0:
                                answer += worddiffb - worddiffa
                                line_list.append(linestart + "-" + str(int(lineend) + answer))
                            elif worddiffb - worddiffa < 0:
                                answer += worddiffa - worddiffb
                                line_list.append(linestart + "-" + str(int(lineend) + answer))
                            elif worddiffa == worddiffb:
                                answer = 0
                                line_list.append(linestart + "-" + str(int(lineend) + answer))

                            # In the textbox, print out the start line. Colour it in blue in order to make it stand out.
                            li += '<font color="blue">' + '<p>' + 'The line difference starts at :- ' + linestart \
                                  + '</p>' + '</font>'

                            # In the textbox, print out the end line. Colour it in blue in order to make it stand out.
                            li += '<font color="blue">' + '<p>' + 'The line difference ends at line :- ' + \
                                  str(int(lineend) + answer) + '</p>' + '</font>'

                        else:
                            # Put a linebreak in to make the text more readable.
                            li += '<p> &nbsp; </p>'

                            # Determine if the line is an addition or a subtraction.
                            if line.strip().startswith('-') is True:
                                if line.strip("-"):
                                    # Colour the text, strip it of html tags and put it into the li list.
                                    # PLEASE NOTE:-
                                    # It's important to strip the text of html codes because we need it pure to input
                                    # our own codes.
                                    li += '<font color="red">' + '<p>' + self.striphtml(line) + '</p>' + '</font>'
                                    # Increment the addition count by one.
                                    poscount += 1

                                    # Adds the current diff line, minus the diff mark to a list for feature highlighting
                                    # later.
                                    highlight_list.append("-'1'" + line.replace('-', '').strip() + "'1'" + "\n")
                            elif line.strip().startswith('+') is True:
                                if line.strip("+"):
                                    # Colour the text, strip it of html tags and put it into the li list.
                                    li += '<font color="red">' + '<p>' + self.striphtml(line) + '</p>' + '</font>'

                                    # Increment the negative count by one.
                                    negcount += 1

                                    # Adds the current diff line, minus the diff mark to a list for feature highlighting
                                    # later.
                                    highlight_list.append("+'1'" + line.replace('+', '').strip() + "\n")

                    else:
                        # Strip it of html tags and put it into the li list.
                        li += '<p>' + self.striphtml(line) + '</p>'

                return_list = ''

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p> &nbsp; </p>' + '<p>' + '<font color="green">' + 'There were ' + str(negcount) \
                               + ' additions to the saved webpage' + '</font>' + '</p>'

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p>' + '<font color="green">' + 'There were ' + str(poscount) \
                               + ' removals from the saved webpage' + '</font>' + '</p>'

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p>' + '<font color="green">' + 'In total, there were ' + str(poscount + negcount) + \
                               ' changes in total to the original webpage' + '</font>' + '</p>'

                self.returnlist = return_list
                # If there were no pages, just set the page to the default page and return the diff results
                # If there were changes, highlight the changes.
                if poscount + negcount == 0:
                    # Get the string value of the default_page list
                    returnpage = self.liststring(default_page)
                    # Set the differential result viewer to return the differential results.
                    self.diff_display.text_editor.setHtml(return_list)
                    # Show the differential result viewer.
                    self.diff_display.show()
                    # Set the webpage viewer to contain the HTML code of the default_page
                    self.htmlComparison.setHtml(returnpage)
                else:
                    # Setup a string variable to containt the information
                    pagestring = ""
                    newlen = len(default_page)
                    originallen = len(original_page)

                    # Begin the loop to process the default page line by line.
                    # The loop contains both an loop index counter and the actual contents on the index indicated.
                    for index, (line, old_line) in enumerate(zip(default_page, original_page)):
                        # For all the results in the highlight list, start processing them.
                        testval = True

                        for highlight_line in highlight_list:
                            # Strip all false whitespace characters from the current highlight list value and split the
                            # current line evaluated at the comma between addition or subtraction value and the actual
                            # line contents.
                            sub = highlight_line.strip(' ').strip('\n').split("'1'")
                            # If the line is present inside the default page, highlight it.
                            # Else, shove it back into the string.
                            if sub[1].strip() in line:
                                # testval = False
                                # If the line was a subtraction, do this.
                                if sub[0] == '+':
                                    for list_ln in line_list:
                                        val_range = list_ln.split("-")
                                        if int(val_range[0]) < index < int(val_range[1]) + poscount + negcount:
                                            testval = False
                                            # Else, do this if it was an addition.
                                            pagestring += '<p></p>' + '<font color="yellow">' + '\n' + \
                                                                  'Addition to Current Page: -' + '\n' + line + '</font>'

                            elif sub[1].strip() in old_line:
                                # testval = False
                                # If the line was a subtraction, do this.
                                if sub[0] == '-':
                                    for list_ln in line_list:
                                        val_range = list_ln.split("-")
                                        if int(val_range[0]) < index <= int(val_range[1])+ poscount + negcount:
                                            testval = False
                                            pagestring += '<p></p>' + '<font color="red">' + '\n' + \
                                                                  'Subtraction to Current Page: -' + '\n' + old_line + '</font>'
                                # elif sub[0] == '+':
                                #     print("Gubs")
                                # else:
                                #     if originallen > newlen:
                                #         pagestring += old_line
                                #     else:
                                #         pagestring += line
                        if testval:
                            if originallen > newlen:
                                pagestring += old_line
                            else:
                                pagestring += line
                    # Ask the user if they would like to view the results of the differential.
                    msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "View Differential Results",
                                                   "Do you wish to view the Differential Results?",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                    result = msgbox.exec_()
                    # If they say yes, display the differential display dialog and push the webpage changes to the
                    # main webviewer.
                    if result_yes(result):
                        self.diff_display.text_editor.setHtml(return_list + li)
                        self.diff_display.show()
                        self.htmlComparison.setHtml("")
                        self.htmlComparison.setHtml(pagestring)
                    else:
                        # Else just push the webpage to the webpage viewer.
                        self.htmlComparison.setHtml("")
                        self.htmlComparison.setHtml(pagestring)
                end = time.clock()
                print("Program Done")
                print(end - start)
            # If the files don't exist, throw up an error.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Incorrect or Non-existent File Entered.")
            # Debug code
            except Exception as err:
                print(err)

    @staticmethod
    def read_files(file_location):
        compare_file = open(file_location, 'r')
        compare_file.seek(0)
        return compare_file

    @staticmethod
    def save_to_file(file_save_location, web_content):
        # Open up the file at the set location, if no file exists, create it.
        # There maybe file issues depending on if you have access writes to the file.
        f = open(file_save_location, 'wb')
        # Write the information to the new file, overwrite any old data stored within the file.
        f.write(web_content)
        # Close the file once it has been written to.
        f.close()

    # Check if there is any input at all, if not, throw up a warning.
    @staticmethod
    def check_contains(url):
        if len(url) == 0:
            return True
        else:
            return False

    # Convert Diff from Generator Object to String Object
    @staticmethod
    def diffconvert(data):
        """

        :rtype: object
        """
        listone = data.split()
        count = 0
        text = ''
        for lines in listone:
            if '@@' in lines:
                count += 1
                if count % 2 == 0:
                    text += lines + '\n'

            elif '.' in lines:
                text += lines + ' ' + '\n'
            else:
                text += lines + ' '
        return text

    # Returns a string comprised with the contents of a given list
    @staticmethod
    def liststring(data):
        returnstring = ''.join(data)
        return returnstring

    # Strips out HTML Tags from Code prior to being placed within a TextBrowser
    @staticmethod
    def striphtml(data):
        import re
        p = re.compile(r'<.*?>')
        return p.sub('', data)

    # Method allows user to access option form if the connected button is pressed
    def changeoptions(self):
        try:
            # Show the created dialog box if pressed.
            # noinspection PyUnusedLocal
            self.optionsprog = UserOptions(self.optionsdialog, self.username)
            self.optionsdialog.show()
        except Exception as err:
            print(err)

    # Open up another dialog area if pressed.
    def newusercreation(self):
        try:
            # Show the created dialog box if pressed.
            # noinspection PyUnusedLocal
            self.userprog = UserProg(self.userdialog)
            self.userdialog.show()
        except Exception as err:
            print(err)

    # Open up the multiwebsite view if pressed
    def multiwebsiteshow(self):
        try:
            # Show the created dialog if pressed.
            # noinspection PyUnusedLocal
            self.multiprog = MultiView(self.multiview, self.username)
            self.multiview.show()
        except Exception as err:
            print(err)

    # Takes in a file and returns a list of the lines in that file for editing.
    def return_html_list(self, file, checkval):
        """

        :param file:
        :return list:
        """
        # Sets up the list to return to the calling function
        file_container = list()
        try:
            # For the file, read in the lines from the file and cannonise the data.
            for line in file.readlines():
                if checkval == 0:
                    # If the data is a newline or  a whitespace, purge the line from the file.
                    if not ((line.isspace()) or ("&nbsp;" in line)):
                        # Else, append the file stripped of all HTML content.
                        file_container.append(self.striphtml(line))
                    else:
                        file_container.append("\n")
                else:
                    # strip(' \t\n\r')
                    file_container.append(line)
        except Exception as err:
            print(err)
        # Return the finished list
        return file_container

    # Select an element and compare it against another selection of HTML code.
    @staticmethod
    def selectelement():
        # TODO:- Need to find some way to checking for similar but different elements within the code.
        # TODO:- Also, actually fucking do the code for this function.
        print("Gubs")

'''
Allows the the user to change his options saved in the file.
Attempt to save new options when button is pressed.
Attempt to save new options to selected file.
Display confirmation on the success or failure of saving the options.
'''


class UserOptions(Ui_Form):
    def __init__(self, useroptions, username):
        # Setup a basic state for the user.
        Ui_Form.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(useroptions)
        # Save the Username to a class variable for future reference.
        self.username = username
        # Save a reference to the Options Window display to allow the class to close itself.
        self.useroptions = useroptions
        # Link the saveItems button to a method inside the class to be exectued upon button press.
        # obj = self.saveItems.clicked.connect(self.savevariables)


'''
Displays all current websites being watched.
Displays the status of current websites being watched.
Displays when they were last checked.
Allows the user to add or remove websites at choice.
Allows the user to force a recheck before the cycle is complete.
Allows the user to set when to check again.
'''


class MultiView(Ui_MultiView):
    def __init__(self, multidialog, username):
        Ui_MultiView.__init__(self)
        self.setupUi(multidialog)
        self.btn_addnewwebsite.clicked.connect(self.add_new_website)
        self.btn_removewebsite.clicked.connect(self.delete_websites)
        self.btn_forcerefresh.clicked.connect(self.refresh_list)
        self.btn_updatewebsites.clicked.connect(self.update_add_websites)
        # self.actionEditOptions.triggered.connect(self.changeoptions)
        self.username = username
        # Set in Minutes
        self.timer_interval = 15
        self.thread_instance = TimerThread(self)
        self.thread_instance.start()
        self.timers = []
        self.showwebsites()

    def update_add_websites(self):
        websites = json.load(open('MainCode//UserFiles//websitewatch.json', 'r'))

        user_websites = websites[self.username]
        urls = [website.split("'1'")[0] for website in user_websites]
        warns = [website.split("'1'")[2] for website in user_websites]
        temp_list = self.multi_thread_return(urls, warns)

        update_list = list()
        for vals in temp_list:

            if vals[2] != "":
                update_list.append(vals[0] + "'1'" + vals[1] + "'1'" + "Warning, Dynamic Website | ")
            else:
                update_list.append(vals[0] + "'1'" + vals[1])

        new_data = {self.username: update_list}
        websites.update(new_data)
        json.dump(websites, open('MainCode//UserFiles//websitewatch.json', 'w'))
        self.clear_lines(self)
        self.showwebsites()

    # Add new websites to the list for observation.
    def add_new_website(self, text):
        try:
            new_website = str()
            if text is not False:
                new_website = text
            else:
                new_website = self.line_addnewwebsite.text()

            websites = json.load(open('MainCode//UserFiles//websitewatch.json', 'r'))
            new_list = websites[self.username]
            templist = [website.split("'1'")[0] for website in new_list]
            if new_website not in templist:
                head = web_content_return(new_website, var="Header")
                new_head = web_content_return(new_website, var="Header")
                if head != new_head:
                    new_list.append(new_website + "'1'" + head + "'1'" + "Warning, Dynamic Website | ")
                else:
                    new_list.append(new_website + "'1'" + head + "'1'" + "")
                new_data = {self.username: new_list}
                websites.update(new_data)
                json.dump(new_data, open('MainCode//UserFiles//websitewatch.json', 'w'))
                self.clear_lines(self)
                self.showwebsites()
            else:
                self.clear_lines(self)
                self.showwebsites()
        except Exception as err:
            print(err)

    # Delete a website from the Python List
    def delete_websites(self):
        try:
            remove_website = self.line_removewebsite.text()
            websites = json.load(open('MainCode//UserFiles//websitewatch.json', 'r'))
            new_list = websites[self.username]
            # templist, headlist = [website.split("'1'")[0][1] for website in new_list]
            for index, line in enumerate(new_list):
                templine = line.split("'1'")
                if remove_website in templine:
                    new_list.pop(index)
            new_data = {self.username: new_list}
            websites.update(new_data)
            json.dump(new_data, open('MainCode//UserFiles//websitewatch.json', 'w'))
            self.clear_lines(self)
            self.showwebsites()
        except Exception as err:
            print(err)

    # Force the list to refresh and update the status of the websites
    def refresh_list(self):
        self.showwebsites()

    def thread_func(self):
        timer = QtCore.QTimer()
        timer.timeout.connect(self.showwebsites)
        timer.start((self.timer_interval * 1000) * 60)
        self.timers.append(timer)

    # Show the websites on the first run through
    def showwebsites(self):
        try:
            self.tableWidget.setRowCount(0)
            with open('MainCode//UserFiles//websitewatch.json', 'r') as outfile:
                websites = json.load(outfile)
                website_list = websites[self.username]
                if len(website_list) == 0:
                    msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Save New Website",
                                                   "Do you wish to save a Website? " + "\n" + "Warning:-" + "\n" +
                                                   "Entering New Website Is Required to Use MultiView Feature.",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                    result = msgbox.exec_()
                    # If they say yes, display the differential display dialog and push the webpage changes to the
                    # main webviewer.
                    if result_yes(result):
                        text, ok = QtWidgets.QInputDialog.getText(None, 'Input Dialog', 'Enter a Website:-')
                        if ok:
                            self.add_new_website(text)
                    else:
                        QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to enter a "
                                                                               "new website.")
                elif len(website_list) != 0:
                    self.thread = ThreadGather(website_list, self)
                    self.thread.strsignal.connect(self.append_to_table)
                    self.thread.start()
            print("Done")
        except Exception as err:
            print(err)

    def multi_thread_return(self, urls, warns):

        import queue
        import threading
        q = queue.Queue()
        threads = []

        for (url, warn) in zip(urls, warns):
            t = threading.Thread(target=self.fetch_url, args=(url, q, warn))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()
        return [q.get() for _ in range(len(urls))]

    @pyqtSlot(int, list)
    def append_to_table(self, index, table_info):
        try:
            for col_line in range(0, len(table_info)):
                # row first, column second, item appending last.
                self.tableWidget.setItem(index, col_line, table_info[col_line])
            self.tableWidget.resizeColumnsToContents()
        except Exception as err:
            print(err)

    @staticmethod
    def clear_lines(self):
        self.line_removewebsite.clear()
        self.line_addnewwebsite.clear()

    @staticmethod
    def fetch_url(url, queue, warn):
        if len(url) == 0:
            print("Empty")
        else:
            temp_url = url
            val = web_content_return(temp_url, var="Thread", warn=warn)
            queue.put(val)
            print(val)

'''
Multiuse method to determine the specific HTML response error gotten from HTML request.
'''


def getresponsecode(url):
    # Pregenerate the URL request prior to calling for it.
    request = Request(url)
    try:
        # noinspection PyUnusedLocal
        # Attempt to open the URL request already generated.
        reponse = urlopen(request)
        return 200
    except urllib.error.HTTPError as err:
        # Error should always be returned, if it is, return the HTML error code recieved.
        return err.code


'''
Returns user save settings as needed.
'''


def retrievesettings(username):
    file = open('MainCode//UserFiles//userdetail.json', 'r')
    user_settings = json.load(file)
    file_location = user_settings[username][0]
    return file_location.get('file_save_location')


def result_yes(result):
    if result == QtWidgets.QMessageBox.Yes:
        return True
    else:
        return False


def username_check(username, jsoninput):
    if username in jsoninput[username][0].get('username'):
        return True


# TODO:- Account for SSL errors if they occur.
def web_content_return(url, var="Url", warn=""):
    if "Url" in var:
        # Attempt to request the html data, pretend to be requesting such data as a firefox browser
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
        # Open the URL as as a separate variable, save the response data.
        html = urlopen(req)
        # Read the data as html data into a separate variable.
        web_content = html.read()
        # Return the web content to the function.
        return web_content

    elif "Thread" in var:
        # Attempt to request the html data, pretend to be requesting such data as a firefox browser
        request = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
        # Open the URL as as a separate variable, save the response data.
        response = urlopen(request)
        # Read the data as html data into a separate variable.
        web_content = response.read()
        # soup = BeautifulSoup(web_content, "html.parser")
        # title = soup.title.text
        head = str(len(web_content))
        # Return the web content to the function.
        return url, head, warn

    elif "Header" in var:
        # Attempt to request the html data, pretend to be requesting such data as a firefox browser
        request = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
        # Open the URL as as a separate variable, save the response data.
        response = urlopen(request)
        # Read the data as html data into a separate variable.
        web_content = response.read()
        head = str(len(web_content))
        # Return the web content to the function.
        return head

'''
If file is called with __main__, then execute the main function and being the script
'''
if __name__ == '__main__':
    main()
