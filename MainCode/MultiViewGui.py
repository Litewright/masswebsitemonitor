from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from urllib.request import *
from bs4 import BeautifulSoup


class ThreadGather(QtCore.QThread):
    strsignal = pyqtSignal(int, list)

    def __init__(self, websitelist, guitable):
        QtCore.QThread.__init__(self)
        self.website_list = websitelist
        self.guittable = guitable

    def __del__(self):
        self.wait()

    def run(self):

        import time
        # start = time.time()

        new_website_list, head_list, warning_list = self.split_by_comma(self.website_list)
        start = time.time()
        temp_list = self.multi_thread_return(new_website_list, head_list, warning_list)

        for index, line in enumerate(temp_list):
            self.guittable.tableWidget.insertRow(index)
            table_info = self.populate_table_list(line[1], line[0], line[2])
            self.strsignal.emit(index, table_info)

        # print("Elapsed Time: %s" % (time.time() - start))

    def multi_thread_return(self, urls, heads, warns):

        import queue
        import threading
        q = queue.Queue()
        threads = []

        for url, head, warn in zip(urls, heads, warns):
            t = threading.Thread(target=self.fetch_url, args=(url, q, head, warn))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()
        return [q.get() for _ in range(len(urls))]

    def populate_table_list(self, line, title, head):
        import time
        table_info = list()
        new_string = ''.join(line)
        table_info.append(self.return_widget_item(title))
        table_info.append(self.return_widget_item(new_string))
        if head is True:
            table_info.append(self.return_widget_item("Changes To Webpage"))
        elif head is False:
            table_info.append(self.return_widget_item("No Changes"))
        table_info.append(self.return_widget_item(time.strftime("%H:%M:%S")))
        return table_info

    @staticmethod
    def fetch_url(url, queue, head, warn):
        if len(url) == 0:
            print("Empty")
        else:
            val = web_content_return(url, head, warn)
            queue.put(val)
            # print(val)

    @staticmethod
    def return_widget_item(item):
        new_item = QtWidgets.QTableWidgetItem(item)
        return new_item

    @staticmethod
    def split_by_comma(comma_list):
        website_list = list()
        head_list = list()
        warning_list = list()
        for line in comma_list:
            temp = line.strip().split("'1'")
            website_list.append(temp[0])
            head_list.append(temp[1])
            if 3 == len(temp):
                warning_list.append(temp[2])
            else:
                warning_list.append("")
        return website_list, head_list, warning_list


# TODO:- Account for SSL errors if they occur.
def web_content_return(url, head, warn):
    # import ssl
    # context = ssl._create_unverified_context()
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
    response = urlopen(req)
    web_content = response.read()
    soup = BeautifulSoup(web_content, "html.parser")
    title = soup.title.text
    new_head = str(len(web_content))
    if head == new_head:
        return warn + " " + title, url, False
    else:
        return warn + " " + title, url, True
