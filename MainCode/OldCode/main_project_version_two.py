import json
import urllib.error
from difflib import *
from PyQt5 import QtWidgets
from UICode.webwatcher import Ui_webwatcher
from UICode.pythongui import Ui_MainWindow
from UICode.jsoninput import Ui_JSONInput
from UICode.weblogin import Ui_WebLogin
from UICode.options import Ui_Form
from UICode.multiview import Ui_MultiView
from urllib.request import *

'''
Kick the Program off with the Main Function.
Call the MainController Class to hold the upper most level variables.
Control the start and end of the application.
Main is called at the bottom of the file.
'''


def main():
    import sys
    # Defines the application as an app within the windows environment.
    app = QtWidgets.QApplication(sys.argv)
    # noinspection PyUnusedLocal
    # Start the Main controller to start the entire program.
    controller = MainController()
    # Sets the close behaviour of the program.
    sys.exit(app.exec_())


'''
Initalise the login screen.
Bind them to local variables to avoid garbage collection.
Show the login screen.
'''


class MainController:
    def __init__(self):
        # Create a QMainWindow to display the Login Window.
        self.logindialog = QtWidgets.QMainWindow()
        # Initialise the LoginDialog screen with this UI file and Python Class.
        self.loginprog = LoginProg(self.logindialog)
        # Show the login dialog screen.
        self.logindialog.show()


'''
Sets up the LoginScreen for the user to log into.
It checks the json file for matches in username, and then in the password field.
If successful, it'll pass the user onto the main screen.
In future, it'll send the user onto their account to perform diffs and measures
'''


class LoginProg(Ui_WebLogin):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, logindialog):
        # Setup a basic state for the user.
        Ui_WebLogin.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(logindialog)
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of logincheck.
        # Set the password textfield to be a series of password characters over plaintext.
        self.loginPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        # Hold a reference to the QMainWindow that displays the login dialog.
        self.logindialog = logindialog
        # Create a variable to hold a reference to a QMainWindow to hold the GUIProgram.
        self.dialog = QtWidgets.QMainWindow()
        # noinspection PyUnusedLocal
        # Link the login button to the method known as logincheck.
        obj = self.loginButton.clicked.connect(self.logincheck)
        # Preinitalise the variable to hold a reference to the class variable
        self.prog = Ui_MainWindow

    # Function compares the input from the username and password fields to the input from the account json file.
    def logincheck(self):
        # Bind the text from these fields into variables.
        # username = self.loginUsername.text()
        # password = self.loginPassword.text()
        username = "Joshua"
        password = "Password"
        # Open up the userdetail.json file for reading as an outfile.
        # Iterate over the file as needed.
        try:
            with open('userdetail.json', 'r') as outfile:
                # Attempt to perform this action, return an error if it fucks up.
                try:
                    # Load the json file as data.
                    data = json.load(outfile)
                    # Check if the Username is set as a key or not, all user names are set as keys in the program
                    # If the key matches the entered username, check the password.
                    if username in data[username][0].get('username'):
                        # Check if the password matches the recorded password for the username, return with an error
                        # if it does not.
                        if password in data[username][1].get('password'):
                            # Hide the Login Screen
                            self.logindialog.hide()
                            # Initalise the class variable prog with GuiProgram and a few variables.
                            self.prog = GuiProgram(self.dialog, username)
                            # Show the dialog program.
                            self.dialog.show()
                        else:
                            # Inform the User that their Username or Password was incorrect
                            # noinspection PyTypeChecker,PyArgumentList
                            QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match"
                                                              )
                    else:
                        # Inform the User that their Username or Password was incorrect
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match")
                # Print the error that occurred.
                except Exception as err:
                    errtest = err.args[0]
                    if errtest == username:
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match")
                    else:
                        print(err)
        except Exception as err:
            print(err)


'''
Allows the user to add new users as required.
Inputs this into the jsonfile listed based off input into the text fields.
Only has one function.
'''


class UserProg(Ui_JSONInput):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, userdialog):
        # Setup a basic state for the user.
        Ui_JSONInput.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(userdialog)
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of jsoninput.
        # noinspection PyUnusedLocal
        obj = self.jsonInputButton.clicked.connect(self.jsoninput)

    def jsoninput(self):
        # Binds the textfield inputs into variables for processing.
        username = self.inputUsername.text()
        password = self.inputPassword.text()
        # Attempt to add new user to the userjson file.
        try:
            # Open the JSON file for input as dict
            newaccount = json.load(open('userdetail.json'))

            # Check if the current username exists or not.
            # If it doesn't ignore the error, it's expected.
            # If it does, execute the code below.
            try:
                # Set username to equal this value, if the username already exists within the json list
                # noinspection PyUnusedLocal
                testvar = newaccount[username][0].get('username')
                # Ask the User if they'd like to change the current password for that present user.
                msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Change Password",
                                               "Do you wish to change the Password?",
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                # Result is equal to what button the user pressed.
                result = msgbox.exec_()

                # If the user responds with their desire to change the password, do the change method
                if result == QtWidgets.QMessageBox.Yes:
                    self.save_password(username, password, newaccount)
                # Else, return a message to the user that Password was not changed.
                else:
                    # noinspection PyTypeChecker,PyArgumentList
                    QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to change this "
                                                                           "users password.")
            # Catch any errors relating to the fact that the file doesn't exist or if the username isn't present.
            # If the error is an unexpected error, catch it and print the information.
            except Exception as err:
                # If the user doesn't exist, assume that the user wishes to add a new user, and add it.
                errtest = err.args[0]
                if errtest == username:
                    self.save_password(username, password, newaccount)
                else:
                    print(err)
        except Exception as err:
            print(err)

    # Save the Password and Username to the Json file containing usernames and passwords.
    @staticmethod
    def save_password(username, password, newaccount):
        # Bind the password and username into the correct format for usage.
        data = {username: [{'username': username}, {'password': password}]}
        # Append the new data onto the old file data.
        newaccount.update(data)
        # Save the old data + the new data to the json file.
        json.dump(newaccount, open('userdetail.json', 'w'))


'''
Attempt to show a line by line comparison of both files within the program.
Attempt to highlight the key differences of both files.
Attempt to check one specific element of HTML code vs the same element within the file.
'''


class WebProg(Ui_webwatcher):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, webdialog):
        # Setup a basic state for the user.
        Ui_webwatcher.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(webdialog)
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of displayhtml.
        self.pushButton.clicked.connect(self.displayhtml)
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of selectelement.
        self.pushButton_2.clicked.connect(self.selectelement)

    # This function displays the HTML code (of a webpage)
    # to allow you to select a HTML element to watch.
    def displayhtml(self):
        # Set the url as the text contained within the lineEdit.
        url = self.lineEdit_3.text()
        # Check if there is any input at all, if not, throw up a warning.
        if len(url) == 0:
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            # If input exists, attempt to perform the request. Throw up errors if they occur.
            try:
                # Attempt to request the html data, pretend to be requesting such data as a firefox browser
                req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
                # Open the URL as as a separate variable, save the response data.
                html = urlopen(req)
                # Read the data as html data into a separate variable.
                web_content = html.read()
                # Save the web content as plaintext decoded into the utf-8 standard.
                plaintext = web_content.decode("utf-8")
                # Set the htmlViewer to display the text as plaintext
                # If you don't it'll attempt to display it as an actual webpage.
                self.htmlViewer.setPlainText(plaintext)
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Debug Code
            except Exception as err:
                print(err)

    # Select an element and compare it against another selection of HTML code.
    @staticmethod
    def selectelement():
        # TODO:- Need to find some way to checking for similar but different elements within the code.
        # TODO:- Also, actually fucking do the code for this function.
        print("Gubs")


'''
Main computing class as of present.
Handles performing basic diffs on urls and HTML web scraped files.
Allows web pages to be saved to a fixed location.
Allows the current user to move to other screens upon button click
'''


class GuiProgram(Ui_MainWindow):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, dialog, username):

        # Setup a basic state for the user.
        Ui_MainWindow.__init__(self)

        # Setup the linked UI file with the current class.
        self.setupUi(dialog)

        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of save.
        # noinspection PyUnusedLocal
        obj = self.urlButton.clicked.connect(self.save)

        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of compare.
        # noinspection PyUnusedLocal
        obj1 = self.compareButton.clicked.connect(self.compare)
        # Create some windows to allow users to view other forms when specific buttons are pressed.
        self.userdialog = QtWidgets.QMainWindow()
        self.webdialog = QtWidgets.QMainWindow()
        self.optionsdialog = QtWidgets.QMainWindow()
        # Set the class username variable to equal the username given to the class by the Login Screen
        self.username = username
        # Set the save file location to the associated user location from the user options.
        self.file_save_location = self.retrievesettings()
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of changeoptions.
        obj2 = self.actionEditOptions.triggered.connect(self.changeoptions)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of newusercreation.
        obj3 = self.actionAdd_New_User.triggered.connect(self.newusercreation)

    def retrievesettings(self):
        file = open('usersettings.json', 'r')
        user_settings = json.load(file)
        file_location = user_settings[self.username][0]
        return file_location.get('file_save_location')

    # Save the URL put inside lineEdit_2 to a file for further future manipulation.
    def save(self):
        # Set the url as the text contained within the lineEdit.
        # TODO:- Fix the names of the variables on the UI file to something less generic.
        url = self.selectUrl.text()
        # Check if there is any input at all, if not, throw up a warning.
        if len(url) == 0:
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            try:
                # Attempt to request the html data, pretend to be requesting such data as a firefox browser
                req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
                # Open the URL as as a separate variable, save the response data.
                html = urlopen(req)
                # Read the data as html data into a separate variable.
                web_content = html.read()
                # Save the web content as plaintext decoded into the utf-8 standard.
                plaintext = web_content.decode("utf-8")
                # Set the htmlViewer to display the text as plaintext
                # If you don't it'll attempt to display it as an actual webpage.
                self.htmlViewer.setPlainText(plaintext)

                msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Save Webpage",
                                               "Do you wish to save the Webpage?",
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                result = msgbox.exec_()
                if result == QtWidgets.QMessageBox.Yes:
                    # Open up the file at the set location, if no file exists, create it.
                    # There maybe file issues depending on if you have access writes to the file.
                    f = open(self.file_save_location, 'wb')
                    # TODO:- Additional code to handle if the file already exists (such as name changes).
                    # TODO:- Way for user to change the location of saving the file.
                    # Write the information to the new file, overwrite any old data stored within the file.
                    f.write(web_content)
                    # Close the file once it has been written to.
                    f.close()
                else:
                    # noinspection PyTypeChecker,PyArgumentList
                    QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to save the webpage.")
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Respond with a message if the file location is invalid or you do not have user rights to save the file.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "File name or location invalid. Check current settings.")
            # Debug Code
            except Exception as err:
                print(err)

    # Scrape the web content from the given URL and save it.
    # Pass it along to another function to perform the actual differential.
    def compare(self):
        # Set the url as the text contained within the lineEdit.
        # TODO:- Fix the names of the variables on the UI file to something less generic.
        url = "http://gmandam.com/diff-test/"
        file_area = "D:\\Users\\Gmandam\\Desktop\\webpageDownload.html"
        # url = self.selectUrl.text()
        # Set the file area for the already saved file to the input of lineEdit as a string.
        # file_area = self.fileSelect.text()
        # Set the save location for the HTML code scraped to a fixed location
        save_file_location = 'D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html'
        # Check if there is any input at all, if not, throw up a warning.
        if len(url) == 0:
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            try:
                # Attempt to request the html data, pretend to be requesting such data as a firefox browser
                req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
                # Open the URL as as a separate variable, save the response data.
                html = urlopen(req)
                # Read the data as html data into a separate variable.
                web_content = html.read()
                # Open up the file at the set location, if no file exists, create it.
                # There maybe file issues depending on if you have access writes to the file.
                f = open(save_file_location, 'wb')
                # TODO:- Additional code to handle if the file already exists (such as name changes).
                # TODO:- Way for user to change the location of saving the file.
                # Write the information to the new file, overwrite any old data stored within the file.
                f.write(web_content)
                # Close the file once it has been written to.
                f.close()
                # Perform another function to do the actual comparisons.
                self.comparefile(file_area, save_file_location)
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError as err:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
                print(err)
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Respond with an error if the file location is invalid or not.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Incorrect File Name Entered.")
            # Debug Code
            except Exception as err:
                print(err)

    # Performs the actual differential upon the webpage and file.
    # Report on the differences, or the lack of differences.
    def comparefile(self, file_area, save_file_location):
        import time
        start = time.clock()
        print("Start of Time Measure")
        # Check if there is any input at all, if not, throw up a warning.
        if len(file_area) == 0:
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a File Location.")
        else:
            try:
                # Attempt to open the files for read only access in a binary format.
                compare_file = open(file_area, 'r')
                compare_file2 = open(save_file_location, 'r')
                # Setup some lists to hold the html pages as lists.
                file_container_one = self.return_html_list(compare_file, 0)
                file_container_two = self.return_html_list(compare_file2, 0)
                # Return file read curser to the top of the file.
                compare_file2.seek(0)
                default_page = self.return_html_list(compare_file2, 1)

                # Close both open files.
                compare_file.close()
                compare_file2.close()

                # Perform a unix style diff on the files that are open.
                diff = unified_diff(file_container_one, file_container_two, lineterm='')

                # If the second file is closed, delete it from the system.
                # Warning, sometimes there is a permissions error here in some IDEs.
                # if compare_file2.closed is True:
                #    import os
                #    os.remove('D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html')

                # If the Diff is empty, respond saying that there were no changes. Else respond that there
                # were changes.
                text = self.diffconvert(self.liststring(list(diff)))
                print(text)

                # Ready a new list to host the diff output with our changes.
                # Side note, we need to do this so that we can break the diff output down line by line
                # and then edit it inline
                li = ''
                highlight_list = list()
                # Count the number of additions and removals from the source file to the current webpage.
                poscount = 0
                negcount = 0

                # This iterates over every line from the diff output allowing us to determine what exactly what
                # differences exist between the current webpage and the saved copy.
                for index, line in enumerate(text.splitlines()):

                    # If the line starts with a - or +, then perform these actions.
                    # This is important, all changes in the work will be highlighted with either a - or +.
                    # The first line, the one that showcases where the changes take place and the number of changes is
                    # marked with a @.
                    if line.startswith(('-', '+')) is True:

                        # Determine if this is one of the lines that indicates where exactly the changes took place
                        # between the old and new files.
                        # TODO:- Make sure we calcualte the number of @ in this line to determine if we should care
                        # TODO:- about it or not.
                        if '@' in line:

                            # Cleanup the line in order to filter out the unimportant data such as whitespace and the
                            # -,+ and @ characters.
                            cleanline = line.replace('-', '').replace('+', '').replace('@', '').split(' ')

                            # Based off the cleanline list, rip out the values for the line locations that differ
                            # between the files. IMPORTANT NOTE: - The location should in theory always be the same due
                            # to the way the unified diff command works.
                            linestart = str(int(cleanline[2].split(',')[0]) + 1)
                            lineend = str(int(cleanline[3].split(',')[0]) + 1)

                            # Assigns the second half of the initial unified diff output into variables to calculate
                            # the actual end for where the line is.
                            worddiffa = int(cleanline[2].split(',')[1])
                            worddiffb = int(cleanline[3].split(',')[1])

                            # Calculate the actual line end, it's a calculation by removing the larger number from the
                            # smaller number. The logic below determines the larger number in order for the calculation
                            # to make sense.
                            answer = 0
                            if worddiffa - worddiffb < 0:
                                answer += worddiffb - worddiffa
                            elif worddiffb - worddiffa < 0:
                                answer += worddiffa - worddiffb

                            # In the textbox, print out the start line. Colour it in blue in order to make it stand out.
                            li += '<font color="blue">' + '<p>' + 'The line difference starts at :- ' + linestart \
                                  + '</p>' + '</font>'

                            # In the textbox, print out the end line. Colour it in blue in order to make it stand out.
                            li += '<font color="blue">' + '<p>' + 'The line difference ends at line :- ' + \
                                  str(int(lineend) + answer) + '</p>' + '</font>'
                            # highlight_list.append(int(linestart))
                            # highlight_list.append(int(int(lineend) + int(answer)))

                        else:
                            # Put a linebreak in to make the text more readable.
                            li += '<p> &nbsp; </p>'

                            # Determine if the line is an addition or a subtraction.
                            if line.startswith('-') is True:

                                # Colour the text, strip it of html tags and put it into the li list.
                                # PLEASE NOTE:-
                                # It's important to strip the text of html codes because we need it pure to input our
                                # own codes.
                                li += '<font color="red">' + '<p>' + self.striphtml(line) + '</p>' + '</font>'
                                # Increment the addition count by one.
                                poscount += 1
                                # highlight_list.append('-,' + self.striphtml(line).replace('-', '').replace(' ', ''))
                                # highlight_list.append('-')
                                # highlight_list.append(line.replace('-', ''))
                                highlight_list.append('-,' + line.replace('-', ''))
                            elif line.startswith('+') is True:

                                # Colour the text, strip it of html tags and put it into the li list.
                                li += '<font color="red">' + '<p>' + self.striphtml(line) + '</p>' + '</font>'

                                # Increment the negative count by one.
                                negcount += 1
                                # highlight_list.append('+,' + self.striphtml(line).replace('+', '').replace(' ', ''))
                                # highlight_list.append('+')
                                # highlight_list.append(line.replace('+', ''))
                                highlight_list.append('+,' + line.replace('+', ''))

                    else:
                        # Strip it of html tags and put it into the li list.
                        li += '<p>' + self.striphtml(line) + '</p>'

                # Debug code, please ignore.
                # print("Pos Count is :- " + str(poscount))
                # print("Neg Count is :- " + str(negcount))

                return_list = ''

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p> &nbsp; </p>' + '<p>' + '<font color="green">' + 'There were ' + str(negcount) \
                               + ' additions to the saved webpage' + '</font>' + '</p>'

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p>' + '<font color="green">' + 'There were ' + str(poscount) \
                               + ' removals from the saved webpage' + '</font>' + '</p>'

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p>' + '<font color="green">' + 'In total, there were ' + str(poscount + negcount) + \
                               ' changes in total to the original webpage' + '</font>' + '</p>'

                if poscount + negcount == 0:
                    returnpage = self.liststring(default_page)
                    returnpage += return_list
                    self.htmlComparison.setHtml(returnpage)
                else:
                    pagestring = ""
                    for index, line in enumerate(default_page):
                        for i in range(len(highlight_list)):
                            sub = highlight_list[i].strip(' ').split(',')
                            if sub[1] in line:
                                if sub[0] == '-':
                                    pagestring += '<font color="yellow">' + '\n' + 'Subtraction to Current Page,' + \
                                                  '\n' + line + '</font>'
                                else:
                                    pagestring += '<font color="yellow">' + '\n' + 'Addition to Current Page,' + '\n' \
                                                  + line + '</font>'
                            else:
                                pagestring += line

                    pagestring += return_list + li
                    self.htmlComparison.setHtml(pagestring)
                end = time.clock()
                print("Program Done")
                print(end - start)
            # If the files don't exist, throw up an error.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Incorrect or Non-existent File Entered.")
            # Debug code
            except Exception as err:
                print(err)

    # Convert Diff from Generator Object to String Object
    @staticmethod
    def diffconvert(data):
        listone = data.split()
        count = 0
        text = ''
        for lines in listone:
            if '@@' in lines:
                count += 1
                if count % 2 == 0:
                    text += lines + '\n'

            elif '.' in lines:
                text += lines + ' ' + '\n'
            else:
                text += lines + ' '
        return text

    # Returns a string comprised with the contents of a given list
    @staticmethod
    def liststring(data):
        returnstring = ''.join(data)
        return returnstring

    # Strips out HTML Tags from Code prior to being placed within a TextBrowser
    @staticmethod
    def striphtml(data):
        import re
        p = re.compile(r'<.*?>')
        return p.sub('', data)

    # Method allows user to access option form if the connected button is pressed.
    def changeoptions(self):
        try:
            # Show the created dialog box if pressed.
            # noinspection PyUnusedLocal
            optionsprog = UserOptions(self.optionsdialog, self.username)
            self.optionsdialog.show()
        except Exception as err:
            print(err)

    # Open up another dialog area if pressed.
    def newusercreation(self):
        try:
            # Show the created dialog box if pressed.
            # noinspection PyUnusedLocal
            userprog = UserProg(self.userdialog)
            self.userdialog.show()
        except Exception as err:
            print(err)

    # Takes in a file and returns a list of the lines in that file for editing.
    def return_html_list(self, file, checkval):
        """

        :param file:
        :return list:
        """
        # Sets up the list to return to the calling function
        file_container = list()
        try:
            # For the file, read in the lines from the file and cannonise the data.
            for line in file.readlines():
                if checkval == 0:
                    # If the data is a newline or  a whitespace, purge the line from the file.
                    if (line.isspace()) or ("&nbsp;" in line):
                        print("ReplacedLine")
                    else:
                        # Else, append the file stripped of all HTML content.
                        file_container.append(self.striphtml(line))
                else:
                    file_container.append(line)
        except Exception as err:
            print(err)
        # Return the finished list
        return file_container


'''
Allows the the user to change his options saved in the file.
Attempt to save new options when button is pressed.
Attempt to save new options to selected file.
Display confirmation on the success or failure of saving the options.
'''


class UserOptions(Ui_Form):
    def __init__(self, useroptions, username):
        # Setup a basic state for the user.
        Ui_Form.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(useroptions)
        # Save the Username to a class variable for future reference.
        self.username = username
        # Save a reference to the Options Window display to allow the class to close itself.
        self.useroptions = useroptions
        # Link the saveItems button to a method inside the class to be exectued upon button press.
        # obj = self.saveItems.clicked.connect(self.savevariables)


'''
Multiuse method to determine the specific HTML response error gotten from HTML request.
'''


def getresponsecode(url):
    # Pregenerate the URL request prior to calling for it.
    requesttest = Request(url)
    try:
        # noinspection PyUnusedLocal
        # Attempt to open the URL request already generated.
        reponse = urlopen(requesttest)
    except urllib.error.HTTPError as err:
        # Error should always be returned, if it is, return the HTML error code recieved.
        return err.code


'''
If file is called with __main__, then execute the main function and being the script
'''
if __name__ == '__main__':
    main()
