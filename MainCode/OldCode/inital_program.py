from tkinter import *
from tkinter import ttk, messagebox
from urllib.request import urlopen, Request
from difflib import unified_diff
import os


form_class = uic.loadUiType("mainwindow")[0]

__author__ = 'Gmandam'

root = Tk()
root.title("Save and Compare Websites")
root.minsize(150, 150)
root.wm_iconbitmap('Coffee_Acrobat.ico')

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky="N, W, E, S")
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1) 

website_compare = StringVar()
file_compare = StringVar()
is_different = StringVar()


def callback():

    if len(website_compare.get()) == 0:
        messagebox.askokcancel("Quit", "No Input Detected.")
    else:
        try:
            url = website_compare.get()
            req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
            html = urlopen(req)
            web_content = html.read()
            f = open('D:\\Users\\Gmandam\\Desktop\\webpageDownload.html', 'wb')
            f.write(web_content)
            f.close()
        except ValueError:
            messagebox.askokcancel("Quit", "Invalid URL Entered.")


def compare():
    compare_file = ''
    compare_file2 = ''

    if len(website_compare.get()) == 0:
        messagebox.askokcancel("Quit", "No Input Detected.")
    else:
        try:
            url = website_compare.get()
            req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
            html = urlopen(req)
            web_content = html.read()
            f = open('D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html', 'wb')
            f.write(web_content)
            f.close()
            compare_file2 = open('D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html', 'r')

        except ValueError:
            messagebox.askokcancel("Quit", "Invalid URL Entered.")

    if len(file_entry.get()) == 0:
        messagebox.askokcancel("Quit", "No Input Detected.")
    else:
        try:
            file_area = file_entry.get()
            compare_file = open(file_area, 'r')

        except IOError:
            messagebox.askokcancel("Quit", "Invalid File Location Selected")

    diff = unified_diff(compare_file2.readlines(), compare_file.readlines(), lineterm='')

    print(''.join(list(diff)))
    print("It Worked")

    compare_file.close()
    compare_file2.close()
    if compare_file2.closed is True:
        os.remove('D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html')

    if diff == '':
        messagebox.askokcancel("Update", "There are no changes.")
    else:
        messagebox.askokcancel("Update", "There Were Changes.")
        '''TODO:- Find some method of having custom option boxes from messagebox.
        '''

ttk.Label(mainframe, text="Enter URL Here : ").grid(column=1, row=1)
website_entry = ttk.Entry(mainframe, width=50, textvariable=website_compare)
website_entry.grid(column=2, row=1)

ttk.Label(mainframe, text="Enter File Here : ").grid(column=1, row=2)
file_entry = ttk.Entry(mainframe, width=50, textvariable=file_compare)
file_entry.grid(column=2, row=2)

ttk.Button(mainframe, text="Compare", command=compare).grid(column=2, row=3, sticky='E')
ttk.Button(mainframe, text="Save", command=callback).grid(column=2, row=3)

for child in mainframe.winfo_children():
    child.grid_configure(padx=10, pady=10)

root.mainloop()
