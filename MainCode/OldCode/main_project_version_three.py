import json
import urllib.error
from difflib import *
from PyQt5 import QtWidgets, QtCore, QtWebKitWidgets, QtGui
from urllib.request import *
from MainProject.UICode.pythongui import Ui_MainWindow
from MainProject.UICode.jsoninput import Ui_JSONInput
from MainProject.UICode.weblogin import Ui_WebLogin
from MainProject.UICode.options import Ui_Form
from MainProject.UICode.multiview import Ui_MultiView
from bs4 import BeautifulSoup
'''
Kick the Program off with the Main Function.
Call the MainController Class to hold the upper most level variables.
Control the start and end of the application.
Main is called at the bottom of the file.
'''


def main():
    import sys
    # Defines the application as an app within the windows environment.
    app = QtWidgets.QApplication(sys.argv)
    # noinspection PyUnusedLocal
    # Start the Main controller to start the entire program.
    controller = MainController()
    # Sets the close behaviour of the program.
    sys.exit(app.exec_())


'''
Initalise the login screen.
Bind them to local variables to avoid garbage collection.
Show the login screen.
'''


class MainController:
    def __init__(self):
        # Create a QMainWindow to display the Login Window.
        self.logindialog = QtWidgets.QMainWindow()
        # Initialise the LoginDialog screen with this UI file and Python Class.
        self.loginprog = LoginProg(self.logindialog)
        # Show the login dialog screen.
        self.logindialog.show()


'''
Sets up the LoginScreen for the user to log into.
It checks the json file for matches in username, and then in the password field.
If successful, it'll pass the user onto the main screen.
In future, it'll send the user onto their account to perform diffs and measures
'''


class LoginProg(Ui_WebLogin):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, logindialog):
        try:
            # Setup a basic state for the user.
            Ui_WebLogin.__init__(self)
            # Setup the linked UI file with the current class.
            self.setupUi(logindialog)
            # Connect the action listener on the UI file (for the button) to that of a function.
            # The function being that of logincheck.
            # Set the password textfield to be a series of password characters over plaintext.
            self.loginPassword.setEchoMode(QtWidgets.QLineEdit.Password)
            # Hold a reference to the QMainWindow that displays the login dialog.
            self.logindialog = logindialog
            # Create a variable to hold a reference to a QMainWindow to hold the GUIProgram.
            self.dialog = QtWidgets.QMainWindow()
            # noinspection PyUnusedLocal
            # Link the login button to the method known as logincheck.
            obj = self.loginButton.clicked.connect(self.logincheck)
            # Preinitalise the variable to hold a reference to the class variable
            self.prog = Ui_MainWindow
        except Exception as err:
            print(err)

    # Function compares the input from the username and password fields to the input from the account json file.
    def logincheck(self):
        # Bind the text from these fields into variables.
        # username = self.loginUsername.text()
        # password = self.loginPassword.text()
        username = "Joshua"
        password = "Password"

        # Open up the userdetail.json file for reading as an outfile.
        # Iterate over the file as needed.
        try:
            with open('userdetail.json', 'r') as outfile:
                # Attempt to perform this action, return an error if it fucks up.
                try:
                    # Load the json file as data.
                    data = json.load(outfile)
                    # Check if the Username is set as a key or not, all user names are set as keys in the program
                    # If the key matches the entered username, check the password.
                    if username_check(username, data):
                        # Check if the password matches the recorded password for the username, return with an error
                        # if it does not.
                        if self.password_check(username, password, data):
                            # Hide the Login Screen
                            self.logindialog.hide()
                            # Initalise the class variable prog with GuiProgram and a few variables.
                            self.prog = GuiProgram(self.dialog, username)
                            # Show the dialog program.
                            self.dialog.show()
                        else:
                            # Inform the User that their Username or Password was incorrect
                            # noinspection PyTypeChecker,PyArgumentList
                            QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match"
                                                              )
                    else:
                        # Inform the User that their Username or Password was incorrect
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match")
                # Print the error that occurred.
                except Exception as err:
                    errtest = err.args[0]
                    if errtest == username:
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "Username or Password does not match")
                    else:
                        print(err)
        except Exception as err:
            print(err)

    @staticmethod
    def password_check(username, password, jsoninput):
        if password in jsoninput[username][1].get('password'):
            return True


'''
Allows the user to add new users as required.
Inputs this into the jsonfile listed based off input into the text fields.
Only has one function.
'''


class UserProg(Ui_JSONInput):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, userdialog):
        # Setup a basic state for the user.
        Ui_JSONInput.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(userdialog)
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of jsoninput.
        # noinspection PyUnusedLocal
        self.obj = self.jsonInputButton.clicked.connect(self.jsoninput)

    def jsoninput(self):
        # Binds the textfield inputs into variables for processing.
        username = self.inputUsername.text()
        password = self.inputPassword.text()
        # Attempt to add new user to the userjson file.
        try:
            # Open the JSON file for input as dict
            newaccount = json.load(open('userdetail.json'))

            # Check if the current username exists or not.
            # If it doesn't ignore the error, it's expected.
            # If it does, execute the code below.
            try:
                # Set username to equal this value, if the username already exists within the json list
                # noinspection PyUnusedLocal
                if username_check(username, newaccount):
                    # Ask the User if they'd like to change the current password for that present user.
                    msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Change Password",
                                                   "Do you wish to change the Password?",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                    # Result is equal to what button the user pressed.
                    result = msgbox.exec_()
                    # If the user responds with their desire to change the password, do the change method
                    if result_yes(result):
                        self.save_password(username, password, newaccount)
                    # Else, return a message to the user that Password was not changed.
                    else:
                        # noinspection PyTypeChecker,PyArgumentList
                        QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to change this "
                                                                               "users password.")
            # Catch any errors relating to the fact that the file doesn't exist or if the username isn't present.
            # If the error is an unexpected error, catch it and print the information.
            except Exception as err:
                # If the user doesn't exist, assume that the user wishes to add a new user, and add it.
                errtest = err.args[0]
                if errtest == username:
                    self.save_password(username, password, newaccount)
                else:
                    print(err)
        except Exception as err:
            print(err)

    # Save the Password and Username to the Json file containing usernames and passwords.
    @staticmethod
    def save_password(username, password, newaccount):
        # Bind the password and username into the correct format for usage.
        data = {username: [{'username': username}, {'password': password}]}
        # Append the new data onto the old file data.
        newaccount.update(data)
        # Save the old data + the new data to the json file.
        json.dump(newaccount, open('userdetail.json', 'w'))


'''
Main computing class as of present.
Handles performing basic diffs on urls and HTML web scraped files.
Allows web pages to be saved to a fixed location.
Allows the current user to move to other screens upon button click
'''


class GuiProgram(Ui_MainWindow):
    # Setup the initial state of the class.
    # The class takes in a reference to the UI file and a self reference.
    def __init__(self, dialog, username):

        # Setup a basic state for the user.
        Ui_MainWindow.__init__(self)

        # Setup the linked UI file with the current class.
        self.setupUi(dialog)

        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of save.
        # noinspection PyUnusedLocal
        obj = self.urlButton.clicked.connect(self.save)

        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of compare.
        # noinspection PyUnusedLocal
        obj1 = self.compareButton.clicked.connect(self.compare)
        # Create some windows to allow users to view other forms when specific buttons are pressed.
        self.userdialog = QtWidgets.QMainWindow()
        self.userprog = Ui_WebLogin
        self.optionsdialog = QtWidgets.QMainWindow()
        self.optionsprog = Ui_Form
        self.diff_display = WebDialog()
        self.multiview = QtWidgets.QMainWindow()
        self.multiprog = Ui_MultiView
        # Set the class username variable to equal the username given to the class by the Login Screen
        self.username = username
        # Set the save file location to the associated user location from the user options.
        self.file_save_location = retrievesettings(self.username)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of changeoptions.
        obj2 = self.actionEditOptions.triggered.connect(self.changeoptions)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of newusercreation.
        obj3 = self.actionAdd_New_User.triggered.connect(self.newusercreation)
        # noinspection PyUnusedLocal
        # Connect the action listener on the UI file (for the button) to that of a function.
        # The function being that of multiwebsiteview.
        obj4 = self.actionView_Multiwebsites.triggered.connect(self.multiwebsiteshow)

    # Save the URL put inside lineEdit_2 to a file for further future manipulation.
    def save(self):
        # Set the url as the text contained within the lineEdit.
        url = self.selectUrl.text()
        # Check if there is any input at all, if not, throw up a warning.
        if self.check_contains(url):
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            try:
                web_content = web_content_return(url)
                # Save the web content as plaintext decoded into the utf-8 standard.
                plaintext = web_content.decode("utf-8")
                # Set the htmlViewer to display the text as plaintext
                # If you don't it'll attempt to display it as an actual webpage.
                self.htmlViewer.setPlainText(plaintext)
                msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "Save Webpage",
                                               "Do you wish to save the Webpage?",
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                result = msgbox.exec_()
                if result_yes(result):
                    self.save_to_file(self.file_save_location + "\\newsaveplace.html", web_content)
                else:
                    # noinspection PyTypeChecker,PyArgumentList
                    QtWidgets.QMessageBox.information(None, "Information", "You have chosen not to save the webpage.")
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Respond with a message if the file location is invalid or you do not have user rights to save the file.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "File name or location invalid. Check current settings.")
            # Debug Code
            except Exception as err:
                print(err)

    # Scrape the web content from the given URL and save it.
    # Pass it along to another function to perform the actual differential.
    def compare(self):
        # Set the url as the text contained within the lineEdit.
        # TODO:- Fix the names of the variables on the UI file to something less generic.
        url = "http://gmandam.com/diff-test/"
        file_area = "D:\\Users\\Gmandam\\Desktop\\webpageDownload.html"
        # url = self.selectUrl.text()
        # Set the file area for the already saved file to the input of lineEdit as a string.
        # file_area = self.fileSelect.text()
        # Set the save location for the HTML code scraped to a fixed location
        save_file_location = 'D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html'
        # Check if there is any input at all, if not, throw up a warning.
        if self.check_contains(url):
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a URL.")
        else:
            try:
                web_content = web_content_return(url)
                self.save_to_file(save_file_location, web_content)
                # Perform another function to do the actual comparisons.
                self.comparefile(file_area, save_file_location)
            # URL Request returned a HTML Error code such as 404 or 403, give the user the error response code.
            except urllib.error.HTTPError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "HTML Response Code Error: " + getresponsecode(url))
            # If there is no connection on the local computer or the website server doesn't exist, inform the user.
            except urllib.error.URLError as err:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Website Connection Error. "
                                                             "Please check your internet connection and website status."
                                              )
                print(err)
            # If the URL is invalid, or is blank, respond to the user telling them so.
            except ValueError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Invalid or Blank URL Error Entered.")
            # Respond with an error if the file location is invalid or not.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Incorrect File Name Entered.")
            # Debug Code
            except Exception as err:
                print(err)

    # Performs the actual differential upon the webpage and file.
    # Report on the differences, or the lack of differences.
    def comparefile(self, file_area, save_file_location):
        import time
        start = time.clock()
        print("Start of Time Measure")
        # Check if there is any input at all, if not, throw up a warning.
        if self.check_contains(file_area):
            # noinspection PyTypeChecker,PyArgumentList
            QtWidgets.QMessageBox.warning(None, "Warning", "Please Enter a File Location.")
        else:
            try:
                # Attempt to open the files for read only access in a binary format.
                compare_file = self.read_files(file_area)
                compare_file2 = self.read_files(save_file_location)
                # Setup some lists to hold the html pages as lists.
                file_container_one = self.return_html_list(compare_file, 0)
                file_container_two = self.return_html_list(compare_file2, 0)
                # Return file read curser to the top of the file.
                compare_file2.seek(0)
                default_page = self.return_html_list(compare_file2, 1)

                # Close both open files.
                compare_file.close()
                compare_file2.close()

                # Perform a unix style diff on the files that are open.
                diff = unified_diff(file_container_one, file_container_two, lineterm='')

                # If the second file is closed, delete it from the system.
                # Warning, sometimes there is a permissions error here in some IDEs.
                # if compare_file2.closed is True:
                #    import os
                #    os.remove('D:\\Users\\Gmandam\\Desktop\\webpageDownload2.html')

                # If the Diff is empty, respond saying that there were no changes. Else respond that there
                # were changes.
                text = self.diffconvert(self.liststring(list(diff)))
                # Ready a new list to host the diff output with our changes.
                # Side note, we need to do this so that we can break the diff output down line by line
                # and then edit it inline
                li = ''
                highlight_list = list()
                # Count the number of additions and removals from the source file to the current webpage.
                poscount = 0
                negcount = 0

                # This iterates over every line from the diff output allowing us to determine what exactly what
                # differences exist between the current webpage and the saved copy.
                for index, line in enumerate(text.splitlines()):

                    # If the line starts with a - or +, then perform these actions.
                    # This is important, all changes in the work will be highlighted with either a - or +.
                    # The first line, the one that showcases where the changes take place and the number of changes is
                    # marked with a @.
                    if line.startswith(('-', '+')) is True:

                        # Determine if this is one of the lines that indicates where exactly the changes took place
                        # between the old and new files.
                        # TODO:- Make sure we calcualte the number of @ in this line to determine if we should care
                        # TODO:- about it or not.
                        if '@' in line:

                            # Cleanup the line in order to filter out the unimportant data such as whitespace and the
                            # -,+ and @ characters.
                            cleanline = line.replace('-', '').replace('+', '').replace('@', '').split(' ')

                            # Based off the cleanline list, rip out the values for the line locations that differ
                            # between the files. IMPORTANT NOTE: - The location should in theory always be the same due
                            # to the way the unified diff command works.
                            linestart = str(int(cleanline[2].split(',')[0]) + 1)
                            lineend = str(int(cleanline[3].split(',')[0]) + 1)

                            # Assigns the second half of the initial unified diff output into variables to calculate
                            # the actual end for where the line is.
                            worddiffa = int(cleanline[2].split(',')[1])
                            worddiffb = int(cleanline[3].split(',')[1])

                            # Calculate the actual line end, it's a calculation by removing the larger number from the
                            # smaller number. The logic below determines the larger number in order for the calculation
                            # to make sense.
                            answer = 0
                            if worddiffa - worddiffb < 0:
                                answer += worddiffb - worddiffa
                            elif worddiffb - worddiffa < 0:
                                answer += worddiffa - worddiffb

                            # In the textbox, print out the start line. Colour it in blue in order to make it stand out.
                            li += '<font color="blue">' + '<p>' + 'The line difference starts at :- ' + linestart \
                                  + '</p>' + '</font>'

                            # In the textbox, print out the end line. Colour it in blue in order to make it stand out.
                            li += '<font color="blue">' + '<p>' + 'The line difference ends at line :- ' + \
                                  str(int(lineend) + answer) + '</p>' + '</font>'

                        else:
                            # Put a linebreak in to make the text more readable.
                            li += '<p> &nbsp; </p>'

                            # Determine if the line is an addition or a subtraction.
                            if line.startswith('-') is True:

                                # Colour the text, strip it of html tags and put it into the li list.
                                # PLEASE NOTE:-
                                # It's important to strip the text of html codes because we need it pure to input our
                                # own codes.
                                li += '<font color="red">' + '<p>' + self.striphtml(line) + '</p>' + '</font>'
                                # Increment the addition count by one.
                                poscount += 1

                                # Adds the current diff line, minus the diff mark to a list for feature highlighting
                                # later.
                                highlight_list.append('-,' + line.replace('-', ''))
                            elif line.startswith('+') is True:

                                # Colour the text, strip it of html tags and put it into the li list.
                                li += '<font color="red">' + '<p>' + self.striphtml(line) + '</p>' + '</font>'

                                # Increment the negative count by one.
                                negcount += 1

                                # Adds the current diff line, minus the diff mark to a list for feature highlighting
                                # later.
                                highlight_list.append('+,' + line.replace('+', ''))

                    else:
                        # Strip it of html tags and put it into the li list.
                        li += '<p>' + self.striphtml(line) + '</p>'

                return_list = ''

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p> &nbsp; </p>' + '<p>' + '<font color="green">' + 'There were ' + str(negcount) \
                               + ' additions to the saved webpage' + '</font>' + '</p>'

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p>' + '<font color="green">' + 'There were ' + str(poscount) \
                               + ' removals from the saved webpage' + '</font>' + '</p>'

                # Colour the text, strip it of html tags and put it into the li list.
                return_list += '<p>' + '<font color="green">' + 'In total, there were ' + str(poscount + negcount) + \
                               ' changes in total to the original webpage' + '</font>' + '</p>'

                # If there were no pages, just set the page to the default page and return the diff results
                # If there were changes, highlight the changes.
                if poscount + negcount == 0:
                    # Get the string value of the default_page list
                    returnpage = self.liststring(default_page)
                    # Set the differential result viewer to return the differential results.
                    self.diff_display.tex.setHtml(return_list)
                    # Show the differential result viewer.
                    self.diff_display.show()
                    # Set the webpage viewer to contain the HTML code of the default_page
                    self.htmlComparison.setHtml(returnpage)
                else:
                    # Setup a string variable to containt the information
                    pagestring = ""
                    # Begin the loop to process the default page line by line.
                    # The loop contains both an loop index counter and the actual contents on the index indicated.
                    for index, line in enumerate(default_page):
                        # For all the results in the highlight list, start processing them.
                        for highlight_line in highlight_list:
                            # Strip all false whitespace characters from the current highlight list value and split the
                            # current line evaluated at the comma between addition or subtraction value and the actual
                            # line contents.
                            sub = highlight_line.strip(' ').split(',')
                            # If the line is present inside the default page, highlight it.
                            # Else, shove it back into the string.
                            if sub[1] in line:
                                # If the line was a subtraction, do this.
                                if sub[0] == '-':
                                    pagestring += '<font color="yellow">' + '\n' + 'Subtraction to Current Page: -,' + \
                                                  '\n' + line + '</font>'
                                else:
                                    # Else, do this if it was an addition.
                                    pagestring += '<font color="yellow">' + '\n' + 'Addition to Current Page: -' + \
                                                  '\n' + line + '</font>'
                            else:
                                pagestring += line
                    # Ask the user if they would like to view the results of the differential.
                    msgbox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, "View Differential Results",
                                                   "Do you wish to view the Differential Results?",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
                    result = msgbox.exec_()
                    # If they say yes, display the differential display dialog and push the webpage changes to the
                    # main webviewer.
                    if result_yes(result):
                        self.diff_display.text_editor.setHtml(return_list + li)
                        self.diff_display.show()
                        self.htmlComparison.setHtml(pagestring)
                    else:
                        # Else just push the webpage to the webpage viewer.
                        self.htmlComparison.setHtml(pagestring)
                end = time.clock()
                print("Program Done")
                print(end - start)
            # If the files don't exist, throw up an error.
            except IOError:
                # noinspection PyTypeChecker,PyArgumentList
                QtWidgets.QMessageBox.warning(None, "Error", "Incorrect or Non-existent File Entered.")
            # Debug code
            except Exception as err:
                print(err)

    @staticmethod
    def read_files(file_location):
        compare_file = open(file_location, 'r')
        compare_file.seek(0)
        return compare_file

    @staticmethod
    def save_to_file(file_save_location, web_content):
        # Open up the file at the set location, if no file exists, create it.
        # There maybe file issues depending on if you have access writes to the file.
        f = open(file_save_location, 'wb')
        # Write the information to the new file, overwrite any old data stored within the file.
        f.write(web_content)
        # Close the file once it has been written to.
        f.close()

    # Check if there is any input at all, if not, throw up a warning.
    @staticmethod
    def check_contains(url):
        if len(url) == 0:
            return True
        else:
            return False

    # Convert Diff from Generator Object to String Object
    @staticmethod
    def diffconvert(data):
        """

        :rtype: object
        """
        listone = data.split()
        count = 0
        text = ''
        for lines in listone:
            if '@@' in lines:
                count += 1
                if count % 2 == 0:
                    text += lines + '\n'

            elif '.' in lines:
                text += lines + ' ' + '\n'
            else:
                text += lines + ' '
        return text

    # Returns a string comprised with the contents of a given list
    @staticmethod
    def liststring(data):
        returnstring = ''.join(data)
        return str(returnstring)

    # Strips out HTML Tags from Code prior to being placed within a TextBrowser
    @staticmethod
    def striphtml(data):
        import re
        p = re.compile(r'<.*?>')
        return p.sub('', data)

    # Method allows user to access option form if the connected button is pressed
    def changeoptions(self):
        try:
            # Show the created dialog box if pressed.
            # noinspection PyUnusedLocal
            self.optionsprog = UserOptions(self.optionsdialog, self.username)
            self.optionsdialog.show()
        except Exception as err:
            print(err)

    # Open up another dialog area if pressed.
    def newusercreation(self):
        try:
            # Show the created dialog box if pressed.
            # noinspection PyUnusedLocal
            self.userprog = UserProg(self.userdialog)
            self.userdialog.show()
        except Exception as err:
            print(err)

    # Open up the multiwebsite view if pressed
    def multiwebsiteshow(self):
        try:
            # Show the created dialog if pressed.
            # noinspection PyUnusedLocal
            self.multiprog = MultiView(self.multiview, self.username)
            self.multiview.show()
        except Exception as err:
            print(err)

    # Takes in a file and returns a list of the lines in that file for editing.
    def return_html_list(self, file, checkval):
        """

        :param file:
        :return list:
        """
        # Sets up the list to return to the calling function
        file_container = list()
        try:
            # For the file, read in the lines from the file and cannonise the data.
            for line in file.readlines():
                if checkval == 0:
                    # If the data is a newline or  a whitespace, purge the line from the file.
                    if (line.isspace()) or ("&nbsp;" in line):
                        print("ReplacedLine")
                    else:
                        # Else, append the file stripped of all HTML content.
                        file_container.append(self.striphtml(line))
                else:
                    file_container.append(line)
        except Exception as err:
            print(err)
        # Return the finished list
        return file_container

    # Select an element and compare it against another selection of HTML code.
    @staticmethod
    def selectelement():
        # TODO:- Need to find some way to checking for similar but different elements within the code.
        # TODO:- Also, actually fucking do the code for this function.
        print("Gubs")


'''
Produces a Dialog to allow the user to see the Differential Results.
Does so via QMainWindow.
Main Viewer is a WebBrowser.
'''


class WebDialog(QtWidgets.QMainWindow):
    def __init__(self):
        # Setup a basic state for the user
        QtWidgets.QMainWindow.__init__(self)
        # Set the title for the Dialog.
        self.setWindowTitle("Differential Results Viewer")
        # Set the main widget to be a frame, to allow for the automatic resizing of the whole dialog
        self.frame = QtWidgets.QFrame()
        # Set the frame as the Central Widget
        self.setCentralWidget(self.frame)
        # Produce a WebView to allow for HTML to be used when producing my Differential Results
        self.text_editor = QtWebKitWidgets.QWebView(self)
        # Set the size of the widget and the main window
        self.text_editor.resize(400, 300)
        self.resize(400, 300)
        # Put the layout as a HBoxLayout
        self.layout = QtWidgets.QHBoxLayout()
        # Set the layout as our main layout.
        self.frame.setLayout(self.layout)
        # Add our text_editor widget to the layout.
        self.layout.addWidget(self.text_editor)


'''
Allows the the user to change his options saved in the file.
Attempt to save new options when button is pressed.
Attempt to save new options to selected file.
Display confirmation on the success or failure of saving the options.
'''


class UserOptions(Ui_Form):
    def __init__(self, useroptions, username):
        # Setup a basic state for the user.
        Ui_Form.__init__(self)
        # Setup the linked UI file with the current class.
        self.setupUi(useroptions)
        # Save the Username to a class variable for future reference.
        self.username = username
        # Save a reference to the Options Window display to allow the class to close itself.
        self.useroptions = useroptions
        # Link the saveItems button to a method inside the class to be exectued upon button press.
        # obj = self.saveItems.clicked.connect(self.savevariables)


'''
Displays all current websites being watched.
Displays the status of current websites being watched.
Displays when they were last checked.
Allows the user to add or remove websites at choice.
Allows the user to force a recheck before the cycle is complete.
Allows the user to set when to check again.
'''


class MultiView(Ui_MultiView):
    def __init__(self, multidialog, username):
        Ui_MultiView.__init__(self)
        self.setupUi(multidialog)
        self.btn_addnewwebsite.clicked.connect(self.add_new_website)
        self.btn_removewebsite.clicked.connect(self.delete_websites)
        self.btn_forcerefresh.clicked.connect(self.refresh_list)
        # self.actionEditOptions.triggered.connect(self.changeoptions)
        self.username = username
        # self.savelocation = retrievesettings(self, self.username)
        self.showwebsites()

    # Add new websites to the list for observation
    def add_new_website(self):
        print("Gubs")

    # Delete a website from the Python List
    def delete_websites(self):
        print("Gubs")

    # Force the list to refresh and update the status of the websites
    def refresh_list(self):
        print("Gubs")

    # Show the websites on the first run through
    def showwebsites(self):
        try:
            with open('websitewatch.json', 'r') as outfile:
                websites = json.load(outfile)
                import time
                start = time.time()
                website_list = websites[self.username]
                title_list = self.multi_thread_return(website_list)
                for index, (line, title) in enumerate(zip(website_list, title_list)):
                    self.tableWidget.insertRow(index)

                    table_info = self.populate_table_list(index, line, title)

                    self.append_to_table(index, table_info)
            self.tableWidget.resizeColumnsToContents()
            print("Elapsed Time: %s" % (time.time() - start))
        except Exception as err:
            print(err)

    def populate_table_list(self, index, line, title):
        table_info = list()
        new_string = ''.join(line)
        # title = self.return_title(new_string)
        table_info.append(self.return_widget_item(str(index + 1)))
        table_info.append(self.return_widget_item(title))
        table_info.append(self.return_widget_item(new_string))
        table_info.append(self.return_widget_item("Good"))
        table_info.append(self.return_widget_item("Now"))
        return table_info

    def multi_thread_return(self, urls):
        import queue
        import threading
        q = queue.Queue()
        threads = []

        for url in urls:
            t = threading.Thread(target=self.fetch_url, args=(url, q))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()

        '''
        threads = [threading.Thread(target=self.fetch_url, args=(url,)) for url in urls]
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        '''

        return [q.get() for _ in range(len(urls))]

    def fetch_url(self, url, queue):
        html = web_content_return(url)
        soup = BeautifulSoup(html, "html.parser")
        title = soup.title.text
        queue.put(title)

    def append_to_table(self, index, table_info):
        try:
            for col_line in range(0, len(table_info)):
                # row first, column second, item appending last.
                self.tableWidget.setItem(index, col_line, table_info[col_line])
        except Exception as err:
            print(err)

    @staticmethod
    def return_widget_item(item):
        new_item = QtWidgets.QTableWidgetItem(item)
        return new_item
'''
Multiuse method to determine the specific HTML response error gotten from HTML request.
'''


def getresponsecode(url):
    # Pregenerate the URL request prior to calling for it.
    request = Request(url)
    try:
        # noinspection PyUnusedLocal
        # Attempt to open the URL request already generated.
        reponse = urlopen(request)
        return 200
    except urllib.error.HTTPError as err:
        # Error should always be returned, if it is, return the HTML error code recieved.
        return err.code


'''Returns user save settings as needed.'''


def retrievesettings(username):
    file = open('usersettings.json', 'r')
    user_settings = json.load(file)
    file_location = user_settings[username][0]
    return file_location.get('file_save_location')


def result_yes(result):
    if result == QtWidgets.QMessageBox.Yes:
        return True
    else:
        return False


def username_check(username, jsoninput) :
    if username in jsoninput[username][0].get('username'):
        return True


def web_content_return(url):
    # Attempt to request the html data, pretend to be requesting such data as a firefox browser
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0 Firefox/41.0'})
    # Open the URL as as a separate variable, save the response data.
    html = urlopen(req)
    # Read the data as html data into a separate variable.
    web_content = html.read()
    # Return the web content to the function.
    return web_content

'''
If file is called with __main__, then execute the main function and being the script
'''
if __name__ == '__main__':
    main()
